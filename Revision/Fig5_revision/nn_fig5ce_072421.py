#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jul 24 13:39:03 2021
@author: cathylengyue
"""       
import tensorflow as tf
import tensorflow.nn as nn
import tensorflow.keras as K
import tensorflow.keras.backend as Kb
#import keras
import numpy as np
import pandas as pd
from sklearn import preprocessing
from sklearn.preprocessing import StandardScaler
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.layers import Input, Dense, Activation
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
import matplotlib.pyplot as plt

def read_data(file_name):
    data = pd.read_excel(io = file_name, header=0)
    return data

def combine_data(files):
    data_list = []
    for file in files:
        data = read_data(file)
        data_list.append(data)
    combined_data = pd.concat(data_list, ignore_index=True)
    return combined_data

def clean_data(data, dof = False):
    data = data.dropna(axis=0) 
    data = data[abs(data['phi_1_unRestrict']) != 0.0000]
    data = data[abs(data['phi_2_unRestrict']) != 0.0000]
    data = data[abs(data['phi_1_unRestrict']) < 30] 
    data = data[abs(data['phi_2_unRestrict']) < 30] 
    data = data[abs(data['phi_1_Restrict']) < 30] 
    data = data[abs(data['phi_2_Restrict']) < 30] 
    if dof: data = data[abs(data['percent_inner_node']) >= 0.55] 
    return data
     
def X_Y_data(data):
    target = ['phi_1_unRestrict','phi_2_unRestrict']

    indy_variable = [
             'fiber_diameter',
             'percent_volume_fraction',
             'I1',
             'I2']
    
    X = data[indy_variable].values
    Y = data[target].values
    return X,Y

train_data = read_data('networks_scaled_optimized_dri.xlsx') #read_data('biaxial_network_scaled.xlsx') #
train_data = clean_data(train_data, dof= False)

train_X, train_Y = X_Y_data(train_data)
scaler = StandardScaler()
scaler_mean = np.mean(train_X,axis=0)  # same as scaler.mean_ array([0.1625825 , 0.45157744, 3.18819534, 3.22309462])
scaler_std = np.std(train_X,axis=0)    # np.sqrt(scaler.var_) array([0.08750812, 0.28193681, 0.12485259, 0.16435753])
scaler.fit(train_X)
train_X = scaler.transform(train_X)

scaleI1 = scaler_std[2]
scaleI2 = scaler_std[3]
scaleI1_inv = 1.0 / scaler_std[2]
scaleI2_inv = 1.0 / scaler_std[3]

import os
rootdir = './convex_tape'
model_paths = [x[1] for x in os.walk(rootdir)][0]

biaxial_data = read_data('biaxial_network_scaled.xlsx')
biaxial_data = clean_data(biaxial_data,dof = False)   
biaxial_X, biaxial_Y = X_Y_data(biaxial_data)
biaxial_X = scaler.transform(biaxial_X)

from matplotlib.offsetbox import TextArea, DrawingArea, OffsetImage, AnnotationBbox
import matplotlib.image as mpimg
import matplotlib.colors as mcolors
import matplotlib.cm as cm

for path in model_paths:
    model_path = './convex_tape/'+ path
    print(model_path)
    #model = keras.models.load_model(model_path, compile=False)
    model = K.models.load_model(model_path, compile=False)
    #model.compile(optimizer=keras.optimizers.Adam(learning_rate=0.0001))
    model.compile(optimizer=K.optimizers.Adam(learning_rate=0.0001))
    
    pred_biaxial_Y = model.predict(biaxial_X[:])
    difference = abs(100*(biaxial_Y - pred_biaxial_Y)/(biaxial_Y + 1e-5))
    mean_difference = np.mean(difference, axis = 1)
    
    biaxial_data_copy = biaxial_data.copy()
    biaxial_data_copy["mean_error"] = pd.DataFrame(data = mean_difference) # 因为mean_difference的index是连续的，而contour_data_copy因为之前删除过data，index可能没有对上
    biaxial_data_copy["pred_psi_1"] = pd.DataFrame(data = pred_biaxial_Y[:,0])
    biaxial_data_copy["pred_psi_2"] = pd.DataFrame(data = pred_biaxial_Y[:,1])
    df = biaxial_data_copy[biaxial_data_copy['lambdaY'] == 1]
    unique_seed_num = sorted(list(df.seeds.unique()))
    unique_cube_len = sorted(list(df.cube_length.unique()))
    
    
    title_font = {'fontname':'Arial', 'size':'15', 'color':'black', 'weight':'normal',
                  'verticalalignment':'bottom'} # Bottom vertical alignment for more space
    axis_font = {'fontname':'Arial', 'size':'13'}
    rgb = ['b', 'g', 'r', 'c', 'y']
    x_axis = np.round(np.arange(1,1.251,0.025), decimals=3)
    
    mus = [1,2,3,4,5]
    colorparams = mus
    #colormap = cm.viridis
    colormap = cm.coolwarm
    normalize = mcolors.Normalize(vmin=np.min(colorparams), vmax=np.max(colorparams))
    rgb = [colormap(normalize(mu)) for mu in mus]
    fig, ax = plt.subplots()
    for idx, i in enumerate(unique_cube_len[:]):
        psi_1_mean = []
        psi_1_std = []
        psi_1_pred_mean = []
        psi_1_pred_std = []
        psi_2_mean = []
        psi_2_std = []
        psi_2_pred_mean = []
        psi_2_pred_std = []
        for j in x_axis:
            psi_1_mean.append(df[(df['lambdaX'] == j) & (df['cube_length'] == i)]['phi_1_unRestrict'].mean()) 
            psi_1_std.append(df[(df['lambdaX'] == j) & (df['cube_length'] == i)]['phi_1_unRestrict'].std())
            psi_1_pred_mean.append(df[(df['lambdaX'] == j) & (df['cube_length'] == i)]['pred_psi_1'].mean()) 
            psi_1_pred_std.append(df[(df['lambdaX'] == j) & (df['cube_length'] == i)]['pred_psi_1'].std())
            psi_2_mean.append(df[(df['lambdaX'] == j) & (df['cube_length'] == i)]['phi_2_unRestrict'].mean()) 
            psi_2_std.append(df[(df['lambdaX'] == j) & (df['cube_length'] == i)]['phi_2_unRestrict'].std())
            psi_2_pred_mean.append(df[(df['lambdaX'] == j) & (df['cube_length'] == i)]['pred_psi_2'].mean()) 
            psi_2_pred_std.append(df[(df['lambdaX'] == j) & (df['cube_length'] == i)]['pred_psi_2'].std())
    
        psi_1_confidence = 1.96*np.array(psi_1_std)
        psi_1_down = np.array(psi_1_mean) - np.array(psi_1_confidence)
        psi_1_up = np.array(psi_1_mean) + np.array(psi_1_confidence)
        psi_2_confidence = 1.96*np.array(psi_2_std)
        psi_2_down = np.array(psi_2_mean) - np.array(psi_2_confidence)
        psi_2_up = np.array(psi_2_mean) + np.array(psi_2_confidence)
    
        # get 5C # need to comment 'get 3E part, otherwise the plotted figure will contain both part'
        plt.plot(x_axis, psi_1_pred_mean, linestyle='-', color = rgb[idx])
        plt.plot(x_axis, psi_1_mean, color=rgb[idx], linestyle='--')
        plt.fill_between(x_axis, psi_1_down, psi_1_up, alpha=0.2, edgecolor=rgb[idx], facecolor=rgb[idx])
        # get 5E
        plt.plot(x_axis, psi_2_pred_mean, linestyle='-', color = rgb[idx])
        plt.plot(x_axis, psi_2_mean, color=rgb[idx], linestyle='--')
        plt.fill_between(x_axis, psi_2_down, psi_2_up, alpha=0.2, edgecolor=rgb[idx], facecolor=rgb[idx])
    
    textstr = '\n'.join(('\u03F4' +' ~ 0.3%', '\u03C6 = 0.1 ' + '\u03BC' + 'm'))
    props = dict(boxstyle='round', facecolor='firebrick', alpha=0.5)
    ax.text(0.05, 0.95, textstr, transform=ax.transAxes, fontsize=12, verticalalignment='top', bbox=props)
    
        # get 5C 
    plt.ylabel(r'$\mathrm{\psi_12}$ (KJ/$m^{3}$)',**axis_font)
    #plt.title('Accuracy of FCNN in strip-biaxial predicting $\mathrm{\psi_1}$',**title_font)
        # get 5E
    #plt.ylabel(r'$\mathrm{\psi_2}$ (KJ/$m^{3}$)',**axis_font)
    #plt.title('Accuracy of FCNN in strip-biaxial predicting $\mathrm{\psi_2}$',**title_font)
    
    plt.xlabel(r'$\mathrm{\lambda_x}$',**axis_font )
    newList = []
    for i in unique_seed_num: 
        newList.append(i)
        newList.append(i)
    plt.legend(newList, loc='upper right', title = 'Seeds in DFN')
    
        # get 5C
    plt.savefig('{}/Fig5CE_scaled.pdf'.format(model_path), dpi = 400, bbox_inches='tight')
        # get 5E
    #plt.savefig('Fig5E_scaled.pdf', dpi = 500, bbox_inches='tight')
    #plt.show()
    plt.close()
    