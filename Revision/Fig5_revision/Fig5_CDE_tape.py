
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct  3 23:29:10 2020
@author: cathylengyue
"""

import tensorflow as tf
import tensorflow.nn as nn
import tensorflow.keras as K
import tensorflow.keras.backend as Kb
#import keras
import numpy as np
import pandas as pd
from sklearn import preprocessing
from sklearn.preprocessing import StandardScaler
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.layers import Input, Dense, Activation
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
import matplotlib.pyplot as plt

from matplotlib.offsetbox import TextArea, DrawingArea, OffsetImage, AnnotationBbox
import matplotlib.image as mpimg
import matplotlib.colors as mcolors
import matplotlib.cm as cm

title_font = {'fontname':'Arial', 'size':'15', 'color':'black', 'weight':'normal',
              'verticalalignment':'bottom'} # Bottom vertical alignment for more space
axis_font = {'fontname':'Arial', 'size':'13'}


def read_data(file_name):
    data = pd.read_excel(io = file_name, header=0)
    return data

def combine_data(files):
    data_list = []
    for file in files:
        data = read_data(file)
        data_list.append(data)
    combined_data = pd.concat(data_list, ignore_index=True)
    return combined_data

def clean_data(data, dof = True):
    data = data.dropna(axis=0) 
    data = data[abs(data['phi_1_unRestrict']) != 0.0000]
    data = data[abs(data['phi_2_unRestrict']) != 0.0000]
    data = data[abs(data['phi_1_unRestrict']) < 30] 
    data = data[abs(data['phi_2_unRestrict']) < 30] 
    data = data[abs(data['phi_1_Restrict']) < 30] 
    data = data[abs(data['phi_2_Restrict']) < 30] 
    if dof: data = data[abs(data['percent_inner_node']) >= 0.55] 
    #print('filtered by value:',len(data)) 
    return data
     
def X_Y_data(data):
    target = ['phi_1_unRestrict','phi_2_unRestrict']

    indy_variable = [
             'fiber_diameter',
             'percent_volume_fraction',
             'I1',
             'I2']
    
    X = data[indy_variable].values
    Y = data[target].values
    return X,Y

train_data = read_data('networks_scaled_optimized_dri.xlsx') 
train_data = clean_data(train_data, dof = False)
train_X, train_Y = X_Y_data(train_data)
scaler_mean = np.mean(train_X,axis=0)  # same as scaler.mean_ array([0.1625825 , 0.45157744, 3.18819534, 3.22309462])
scaler_std = np.std(train_X,axis=0)   # np.sqrt(scaler.var_) array([0.08750812, 0.28193681, 0.12485259, 0.16435753])
scaler = StandardScaler()  
scaler.fit(train_X)
train_X = scaler.transform(train_X)

scaleI1 = scaler_std[2]
scaleI2 = scaler_std[3]
scaleI1_inv = 1.0 / scaler_std[2]
scaleI2_inv = 1.0 / scaler_std[3]


a1 = 10**5
a2 = 10**5
def _loss_tensor(y_true, y_pred, x_train):
    
    MAPELoss = 1 * K.mean(abs(100*(y_true - y_pred)/y_true), axis=-1)
    MAELoss = 1 * K.mean(abs(y_true - y_pred), axis=-1)
    MSELoss = 1 * K.mean(K.square(y_true - y_pred), axis=-1)
    
    MAPELoss_psi2 = 1 * K.mean(abs(100*(y_true[:, 1] - y_pred[:, 1])/y_true[:, 1]), axis=-1)
    MSELoss_psi2 = 5 * K.mean(K.square(y_true[:, 1] - y_pred[:, 1]), axis=-1)
    MAELoss_psi2 = 100 * K.mean(abs(y_true[:, 1] - y_pred[:, 1]), axis=-1)

    MAPELoss_psi1 = 1 * K.mean(abs(100*(y_true[:, 0] - y_pred[:, 0])/y_true[:, 0]), axis=-1)
    MSELoss_psi1 = 100 * K.mean(K.square(y_true[:, 0] - y_pred[:, 0]), axis=-1)
    MAELoss_psi1 = 10 * K.mean(abs(y_true[:, 0] - y_pred[:, 0]), axis=-1)
    
    with tf.GradientTape() as tape:
        term = K.gradients(model.output, model.input)[0]
    
    dPsidI1dI1, dPsidI1dI2, dPsidI2dI1, dPsidI2dI2 = term[2][0],term[3][0],term[2][1],term[3][1]
    offdiag_mean = (dPsidI1dI2 + dPsidI2dI1)/2.0
    PosDefLoss_1 = K.max([0, -dPsidI1dI1])
    
    PosDefLoss_2 = K.max([0, -(dPsidI1dI1 * dPsidI2dI2 - offdiag_mean * offdiag_mean)])
    SymmLoss = abs(dPsidI1dI2 - dPsidI2dI1)
    
    psi1Loss = MAPELoss_psi1 + MSELoss_psi1 + MAELoss_psi1
    psi2Loss = MAPELoss_psi2 + MSELoss_psi2 + MAELoss_psi2
    
    return psi1Loss + psi2Loss + a1 * (PosDefLoss_1 + PosDefLoss_2) + a2 * SymmLoss
    
def loss_func(x_train):
        def loss(y_true,y_pred):
            return _loss_tensor(y_true, y_pred, x_train)
        return loss

def convexityScore(y_true, y_pred):
    term = K.gradients(y_pred,model.input)[0] 
    dPsidI1dI1, dPsidI1dI2, dPsidI2dI1, dPsidI2dI2 = term[2][0],term[3][0],term[2][1],term[3][1]
    offdiag_mean = (dPsidI1dI2 + dPsidI2dI1)/2.0
    PosDefLoss_1 = K.max([0, -dPsidI1dI1])
    PosDefLoss_2 = K.max([0, -(dPsidI1dI1 * dPsidI2dI2 - offdiag_mean * offdiag_mean)])
    return PosDefLoss_1 + PosDefLoss_2

def symmetricScore(y_true, y_pred):
    term = K.gradients(y_pred,model.input)[0] 
    dPsidI1dI1, dPsidI1dI2, dPsidI2dI1, dPsidI2dI2 = term[2][0],term[3][0],term[2][1],term[3][1]
    SymmLoss = abs(dPsidI1dI2 - dPsidI2dI1)
    return SymmLoss

def mae_psi1(y_true, y_pred):
        return abs(y_true[:, 0] - y_pred[:, 0])
    
def mae_psi2(y_true, y_pred):
        return abs(y_true[:, 1] - y_pred[:, 1])

def mse_psi1(y_true, y_pred):
        return (y_true[:, 0] - y_pred[:, 0])**2
    
def mse_psi2(y_true, y_pred):
        return (y_true[:, 1] - y_pred[:, 1])**2

def mape_psi1(y_true, y_pred):
        return abs(100*(y_true[:, 0] - y_pred[:, 0])/y_true[:, 0])
    
def mape_psi2(y_true, y_pred):
        return abs(100*(y_true[:, 1] - y_pred[:, 1])/y_true[:, 1])
        
model_path = 'scaledall_0_0_mix_1110_00100'
#model_path = './convex_tape/scaledall_0_0_100_100_1_0_0_0'
print(model_path)
model = K.models.load_model(model_path, compile=False)
model.compile(optimizer=K.optimizers.Adam(learning_rate=0.0001))



#%%
################### fig 5C 5E. FCNN error biaxial # 5 pairs, each has 20 RVE (lambdaY = 1, strip biaxial)
biaxial_data = read_data('networks_theta_0.3_scaled_optimized_dri.xlsx')
biaxial_data = clean_data(biaxial_data,dof = False)   
biaxial_X, biaxial_Y = X_Y_data(biaxial_data)

biaxial_X = scaler.transform(biaxial_X)

pred_biaxial_Y = model.predict(biaxial_X[:])
difference = abs(100*(biaxial_Y - pred_biaxial_Y)/(biaxial_Y + 1e-5))
mean_difference = np.mean(difference, axis = 1)

biaxial_data_copy = biaxial_data.copy()
biaxial_data_copy["mean_error"] = pd.DataFrame(data = mean_difference) # 因为mean_difference的index是连续的，而contour_data_copy因为之前删除过data，index可能没有对上
biaxial_data_copy["pred_psi_1"] = pd.DataFrame(data = pred_biaxial_Y[:,0])
biaxial_data_copy["pred_psi_2"] = pd.DataFrame(data = pred_biaxial_Y[:,1])
df = biaxial_data_copy[biaxial_data_copy['lambdaY'] == 1]
unique_seed_num = sorted(list(df.seeds.unique()))
unique_cube_len = sorted(list(df.cube_length.unique()))

rgb = ['b', 'g', 'r', 'c', 'y']
x_axis = np.round(np.arange(1,1.251,0.025), decimals=3)
mus = [1,2,3,4,5]
colorparams = mus
#colormap = cm.viridis
colormap = cm.coolwarm
normalize = mcolors.Normalize(vmin=np.min(colorparams), vmax=np.max(colorparams))
rgb = [colormap(normalize(mu)) for mu in mus]
#%%
fig, ax = plt.subplots(2,1,figsize=(6,9))
for idx, i in enumerate(unique_cube_len[:]):
    psi_1_mean = []
    psi_1_std = []
    psi_1_pred_mean = []
    psi_1_pred_std = []
    psi_2_mean = []
    psi_2_std = []
    psi_2_pred_mean = []
    psi_2_pred_std = []
    for j in x_axis:
        psi_1_mean.append(df[(df['lambdaX'] == j) & (df['cube_length'] == i)]['phi_1_unRestrict'].mean()) 
        psi_1_std.append(df[(df['lambdaX'] == j) & (df['cube_length'] == i)]['phi_1_unRestrict'].std())
        psi_1_pred_mean.append(df[(df['lambdaX'] == j) & (df['cube_length'] == i)]['pred_psi_1'].mean()) 
        psi_1_pred_std.append(df[(df['lambdaX'] == j) & (df['cube_length'] == i)]['pred_psi_1'].std())
        psi_2_mean.append(df[(df['lambdaX'] == j) & (df['cube_length'] == i)]['phi_2_unRestrict'].mean()) 
        psi_2_std.append(df[(df['lambdaX'] == j) & (df['cube_length'] == i)]['phi_2_unRestrict'].std())
        psi_2_pred_mean.append(df[(df['lambdaX'] == j) & (df['cube_length'] == i)]['pred_psi_2'].mean()) 
        psi_2_pred_std.append(df[(df['lambdaX'] == j) & (df['cube_length'] == i)]['pred_psi_2'].std())

    psi_1_confidence = 1.96*np.array(psi_1_std)
    psi_1_down = np.array(psi_1_mean) - np.array(psi_1_confidence)
    psi_1_up = np.array(psi_1_mean) + np.array(psi_1_confidence)
    psi_2_confidence = 1.96*np.array(psi_2_std)
    psi_2_down = np.array(psi_2_mean) - np.array(psi_2_confidence)
    psi_2_up = np.array(psi_2_mean) + np.array(psi_2_confidence)

    # get 5C # need to comment 'get 3E part, otherwise the plotted figure will contain both part'
    ax[0].plot(x_axis, psi_1_pred_mean, linestyle='-', color = rgb[idx],label='%i'%unique_seed_num[idx])
    ax[0].plot(x_axis, psi_1_mean, color=rgb[idx], linestyle='--', label='%i'%unique_seed_num[idx])
    ax[0].fill_between(x_axis, psi_1_down, psi_1_up, alpha=0.2, edgecolor=rgb[idx], facecolor=rgb[idx])
    

    # get 5E
    ax[1].plot(x_axis, psi_2_pred_mean, linestyle='-', color = rgb[idx],label='%i'%unique_seed_num[idx])
    ax[1].plot(x_axis, psi_2_mean, color=rgb[idx], linestyle='--',label='%i'%unique_seed_num[idx])
    ax[1].fill_between(x_axis, psi_2_down, psi_2_up, alpha=0.2, edgecolor=rgb[idx], facecolor=rgb[idx])
    
# get 5C 
textstr = '\n'.join(('\u03F4' +' ~ 0.3%', '\u03C6 = 0.1 ' + '\u03BC' + 'm'))
props = dict(boxstyle='round', facecolor='firebrick', alpha=0.5)
ax[0].text(0.05, 0.95, textstr, transform=ax[0].transAxes, fontsize=12, verticalalignment='top', bbox=props)
ax[0].set_ylabel(r'$\mathrm{\psi_1}$ (kPa)',**axis_font)
ax[0].set_xlabel(r'$\mathrm{\lambda_x}$',**axis_font )
ax[0].set_title('Accuracy of FCNN in  $\mathrm{\psi_1}$',**title_font)
# get 5E
textstr = '\n'.join(('\u03F4' +' ~ 0.3%', '\u03C6 = 0.1 ' + '\u03BC' + 'm'))
props = dict(boxstyle='round', facecolor='firebrick', alpha=0.5)
ax[1].text(0.05, 0.95, textstr, transform=ax[1].transAxes, fontsize=12, verticalalignment='top', bbox=props)
ax[1].set_ylabel(r'$\mathrm{\psi_2}$ (kPa)',**axis_font)
ax[1].set_xlabel(r'$\mathrm{\lambda_x}$',**axis_font )
ax[1].set_title('Accuracy of FCNN in $\mathrm{\psi_2}$',**title_font)


ax[0].legend( loc='upper right',title='Seeds in DFN')
ax[1].legend( loc='upper right',title='Seeds in DFN')

    # get 5C
plt.tight_layout()
plt.savefig('Fig5CE_july27_strip_tape_nonconvex.pdf', dpi = 300, bbox_inches='tight')

plt.show()

#%%
################### fig 5D 5F. FCNN error contour plot lambdaX = lambdaY = 1.15 equal biaxial
contour_data = read_data('contour_scaled_optimized_dri.xlsx')
contour_data = clean_data(contour_data, dof = False)    
contour_X, contour_Y = X_Y_data(contour_data)
contour_X = scaler.transform(contour_X)


pred_contour_Y = model.predict(contour_X[:])
mape_psi_1_error = abs(100*(contour_Y[:,0] - pred_contour_Y[:,0])/(contour_Y[:,0] + 1e-5))
mae_psi_1_error = abs(contour_Y[:,0] - pred_contour_Y[:,0])
mae_psi_2_error = abs(contour_Y[:,1] - pred_contour_Y[:,1])
largest_indices = np.argsort(-1*mape_psi_1_error)[:10]

contour_data_copy = contour_data.copy()
contour_data_copy["mae_psi_1_error"] = pd.DataFrame(data = mae_psi_1_error)
contour_data_copy["mape_psi_1_error"] = pd.DataFrame(data = mape_psi_1_error) # 因为mean_difference的index是连续的，而contour_data_copy因为之前删除过data，index可能没有对上
contour_data_copy["mae_psi_2_error"] = pd.DataFrame(data = mae_psi_2_error)
uniqueList = set(zip(contour_data_copy.seeds, contour_data_copy.cube_length, contour_data_copy.fiber_diameter))
contour_data_copy["pred_psi_1"] = pd.DataFrame(data = pred_contour_Y[:,0])
contour_data_copy["pred_psi_2"] = pd.DataFrame(data = pred_contour_Y[:,1])
df = contour_data_copy[(contour_data_copy['lambdaX'] == 1.25) & (contour_data_copy['lambdaY'] == 1.25)]
fiber_diam, percent_volume_frac, psi_1_error, psi_2_error = [],[],[],[]
for i,pairs in enumerate(uniqueList):                     
    subdata = df[(df['seeds'] == pairs[0]) & (df['cube_length']== pairs[1]) & (df['fiber_diameter'] == pairs[2])]
    mape_psi_1_err = subdata.mape_psi_1_error.mean()
    mae_psi_1_err = subdata.mae_psi_1_error.mean()
    mae_psi_2_err = subdata.mae_psi_2_error.mean()
    fiber_diam.append(pairs[2])
    percent_volume_frac.append(subdata.percent_volume_fraction.mean())
    #psi_1_error.append(mape_psi_1_err)
    psi_1_error.append(mae_psi_1_err)
    psi_2_error.append(mae_psi_2_err)
fig, ax = plt.subplots(figsize=(4,3))
plt.xlabel('\u03F4 (%)',**axis_font)
plt.ylabel('\u03C6 (' + '\u03BC' +'m)',**axis_font)
plt.title(' $\mathrm{\psi_1}$ error as a function of microstructure',**title_font)
points = plt.scatter(np.array(percent_volume_frac), np.array(fiber_diam), c=np.array(psi_1_error),cmap="coolwarm", s=20)
plt.colorbar(points,extend="max",label=r'$\mathrm{\psi_1}$ MAE (kPa)') # label='relative error %'
plt.clim(vmin=min(psi_1_error), vmax= 1)
plt.savefig('Fig 5D_MAE_july25_equi_tape_nonconvex.pdf', dpi = 300, bbox_inches='tight')  
plt.show()
#plt.close()
fig, ax = plt.subplots(figsize=(4,3))
plt.xlabel('\u03F4 (%)',**axis_font)
plt.ylabel('\u03C6 (' + '\u03BC' +'m)',**axis_font)
plt.title(' $\mathrm{\psi_2}$ error as a function of microstructure',**title_font)
points = plt.scatter(np.array(percent_volume_frac), np.array(fiber_diam), c=np.array(psi_2_error),cmap="coolwarm", s=20)
plt.colorbar(points,extend="max",label=r'$\mathrm{\psi_2}$ MAE (kPa)') # label='relative error %'
plt.clim(vmin=min(psi_2_error), vmax= 0.75)
plt.savefig('Fig 5F_MAE_july25_equi_tape_nonconvex.pdf', dpi = 300, bbox_inches='tight')  
plt.show()
# #plt.close()
