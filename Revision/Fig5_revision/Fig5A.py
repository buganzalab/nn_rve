import pandas as pd
import os
import numpy as np
import sys
import tensorflow.keras
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.layers import Input, Dense, Activation
import tensorflow.keras.backend as K
K.clear_session()
from sklearn import preprocessing
from sklearn.preprocessing import StandardScaler
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import tensorflow as tf
config = tf.compat.v1.ConfigProto()
tf.compat.v1.keras.backend.set_session(tf.compat.v1.Session(config=config))

train_path = './train_test_data/train_data'

train_files = []
for r, d, f in os.walk(train_path):
    for file in f:
        train_files.append(os.path.join(r, file))


test_path = './train_test_data/factor_xy_test'

test_files = []
for r, d, f in os.walk(test_path):
    for file in f:
        test_files.append(os.path.join(r, file))


def read_data(file_name):
    data = pd.read_csv(filepath_or_buffer = file_name, header=0)
    return data


def combine_data(files):
    data_list = []
    for file in files:
        data = read_data(file)
        data_list.append(data)
    combined_data = pd.concat(data_list, ignore_index=True)
    return combined_data

def clean_data(data):
    data = data.drop(['kf = 0.02'], axis=1)
    data = data.dropna(axis=0)
    data = data[data['inner_node %']>=0.55]
    data = data[abs(data['sigma00'])<100]
    data = data[abs(data['sigma11'])<100] 
    return data
def clean_data2(data):
    data = data.drop(['kf = 0.02'], axis=1)
    data = data.dropna(axis=0)
    data = data[abs(data['sigma00'])<100]
    data = data[abs(data['sigma11'])<100] 
    return data   
    

train_data = combine_data(files = train_files)
test_data = combine_data(files = test_files)

train_data = clean_data(train_data)
test_data = clean_data2(test_data)


def X_Y_data(data):

    
    target = ['sigma00','sigma11']
    indy_variable = ['fiber_diam', 'volume_fraction %', 'factorx','factory','?seeds', 'cube_length', ' total_fibers', 'inner_nodes','inner_node %']
    
    X = data[indy_variable].values
    Y = data[target].values
    
    return X,Y
    
train_X, train_Y = X_Y_data(train_data)
test_X, test_Y = X_Y_data(test_data)

scaler = StandardScaler()
scaler.fit(train_X)
test_X = scaler.transform(test_X)



model = keras.models.load_model('dof_lt55_try_8-8-16')
predictions = model.predict(test_X)
rela_error = abs((test_Y - predictions)/(test_Y+1e-5))
loss_and_metrics = model.evaluate(test_X, test_Y, batch_size=3600)

subscript_map = {
    "0": "₀", "1": "₁", "2": "₂", "3": "₃", "4": "₄", "5": "₅", "6": "₆",
    "7": "₇", "8": "₈", "9": "₉", "a": "ₐ", "b": "♭", "c": "꜀", "d": "ᑯ",
    "e": "ₑ", "f": "բ", "g": "₉", "h": "ₕ", "i": "ᵢ", "j": "ⱼ", "k": "ₖ",
    "l": "ₗ", "m": "ₘ", "n": "ₙ", "o": "ₒ", "p": "ₚ", "q": "૧", "r": "ᵣ",
    "s": "ₛ", "t": "ₜ", "u": "ᵤ", "v": "ᵥ", "w": "w", "x": "ₓ", "y": "ᵧ",
    "z": "₂", "A": "ₐ", "B": "₈", "C": "C", "D": "D", "E": "ₑ", "F": "բ",
    "G": "G", "H": "ₕ", "I": "ᵢ", "J": "ⱼ", "K": "ₖ", "L": "ₗ", "M": "ₘ",
    "N": "ₙ", "O": "ₒ", "P": "ₚ", "Q": "Q", "R": "ᵣ", "S": "ₛ", "T": "ₜ",
    "U": "ᵤ", "V": "ᵥ", "W": "w", "X": "ₓ", "Y": "ᵧ", "Z": "Z", "+": "₊",
    "-": "₋", "=": "₌", "(": "₍", ")": "₎"}
sub_trans = str.maketrans(
    ''.join(subscript_map.keys()),
    ''.join(subscript_map.values()))
#Set the font dictionaries (for plot title and axis titles)
title_font = {'fontname':'Arial', 'size':'15', 'color':'black', 'weight':'normal',
              'verticalalignment':'bottom'} # Bottom vertical alignment for more space
axis_font = {'fontname':'Arial', 'size':'13'}


df = test_data.reset_index(drop=True)
unique_cube_len = list(df.cube_length.unique())
factorx_list = list(df.factorx.unique())[0:-1]

pred_y = model.predict(test_X)

from matplotlib.offsetbox import TextArea, DrawingArea, OffsetImage, AnnotationBbox
fig, ax = plt.subplots()
for idx, i in enumerate(unique_cube_len):
    
    y_mean =  []
    y_std = []
    y_mean_ = []
    for factorx in factorx_list:
        index = df[(df['cube_length']==i) & (df['factorx'] == factorx)].index
        m = df[(df['cube_length']==i) & (df['factorx'] == factorx)].sigma00.mean()
        s = df[(df['cube_length']==i) & (df['factorx'] == factorx)].sigma00.std()
        y_mean.append(m)
        y_std.append(s)
        
        m_ = pred_y[index, 0].mean()
        y_mean_.append(m_)
        
    
    y1 = np.array(y_mean) - 1.96*np.array(y_std)
    y2 = np.array(y_mean) + 1.96*np.array(y_std)
    
    plt.plot(np.array(factorx_list)+1, y_mean, color = 'royalblue',linestyle='dashed')#, 'r--')
    plt.fill_between(np.array(factorx_list)+1, y1, y2, alpha=0.2, edgecolor='cornflowerblue', facecolor='cornflowerblue')
    textstr = '\n'.join(('\u03F4' +' ~ 0.3%', '\u03C6 = 0.1 ' + '\u03BC' + 'm'))
    props = dict(boxstyle='round', facecolor='firebrick', alpha=0.2)
    ax.text(0.05, 0.95, textstr, transform=ax.transAxes, fontsize=12,
        verticalalignment='top', bbox=props)
    plt.plot(np.array(factorx_list)+1, y_mean_, color = 'firebrick')
    
    plt.ylabel('\u03C3xx'.translate(sub_trans) +' (kPa)',**axis_font)
    plt.xlabel('\u03BBx'.translate(sub_trans),**axis_font )
    plt.title('Accuracy of FCNN in Biaxial Test',**title_font)

#plt.show()
plt.savefig('Accuracy of FCNN in strip-biaxial loading_updated.pdf', dpi = 500, bbox_inches='tight')
