#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct  3 23:29:10 2020
@author: cathylengyue
"""

import pandas as pd
import os
import numpy as np
import sys
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick


title_font = {'fontname':'Arial', 'size':'15', 'color':'black', 'weight':'normal',
              'verticalalignment':'bottom'} # Bottom vertical alignment for more space
axis_font = {'fontname':'Arial', 'size':'13'}


'''NOTE: Fig4 is the analysis on raw data and not realted to Neural network or predictions, so no DOF restriction applied'''
############################################################################ 


################### fig 4A 4D 4G. Uncertainty in biaxial test of multiple DFN with the same homogenized microstructure (lambda x varies, lambda y == 1)
def read_data(file_name):
    data = pd.read_csv(filepath_or_buffer = file_name, header=0)
    return data
def read_data_xlsx(file_name):
    data = pd.read_excel(io = file_name, header=0)
    return data

filename = 'networks_theta_0.3_scaled_optimized_dri.xlsx'
biaxial_data = read_data_xlsx(filename)
df = biaxial_data[biaxial_data['lambdaY'] == 1] # 1.15
x_axis = np.round(np.arange(1,1.251,0.025), decimals=3)
psi_mean = []
psi_std = []
psi_1_mean = []
psi_1_std = []
psi_2_mean = []
psi_2_std = []
for i in x_axis:
    #print(i, df[df['lambdaX']==i]['total_psif'].mean())
    psi_mean.append(df[df['lambdaX']==i]['psif_scaled'].mean()) 
    psi_std.append(df[df['lambdaX']==i]['psif_scaled'].std())
    psi_1_mean.append(df[df['lambdaX']==i]['phi_1_unRestrict'].mean()) 
    psi_1_std.append(df[df['lambdaX']==i]['phi_1_unRestrict'].std())
    psi_2_mean.append(df[df['lambdaX']==i]['phi_2_unRestrict'].mean()) 
    psi_2_std.append(df[df['lambdaX']==i]['phi_2_unRestrict'].std())
psi_mean[0] = 0
psi_confidence = 1.96*np.array(psi_std)
psi_confidence[0] = 0
psi_down = np.array(psi_mean) - np.array(psi_confidence)
psi_up = np.array(psi_mean) + np.array(psi_confidence)
psi_1_confidence = 1.96*np.array(psi_1_std)
psi_1_down = np.array(psi_1_mean) - np.array(psi_1_confidence)
psi_1_up = np.array(psi_1_mean) + np.array(psi_1_confidence)
psi_2_confidence = 1.96*np.array(psi_2_std)
psi_2_down = np.array(psi_2_mean) - np.array(psi_2_confidence)
psi_2_up = np.array(psi_2_mean) + np.array(psi_2_confidence)

from matplotlib.offsetbox import TextArea, DrawingArea, OffsetImage, AnnotationBbox
import matplotlib.image as mpimg

#%%
fig, ax = plt.subplots(figsize=(4,3))
ax.plot(x_axis, psi_mean, color='royalblue', linestyle='dashed')
ax.fill_between(x_axis, psi_down, psi_up, alpha=0.5, edgecolor='royalblue', facecolor='firebrick')
textstr = '\n'.join(('\u03F4' +' ~ 0.3%', '\u03C6 = 0.1 ' + '\u03BC' + 'm'))
props = dict(boxstyle='round', facecolor='royalblue', alpha=0.5)
ax.text(0.05, 0.95, textstr, transform=ax.transAxes, fontsize=12,
        verticalalignment='top', bbox=props)
plt.ylabel(r'$\mathrm{\psi}$ [kPa]',**axis_font)
plt.title('Strip biaxial test',**title_font)
plt.xlabel(r'$\mathrm{\lambda_x}$',**axis_font )
#plt.show()
plt.savefig('Psi over lambdaX.pdf', dpi = 300, bbox_inches='tight')  # Fig4A
#plt.close()


#%%%
## better to comment 64-75 to run the following code (sometimes, plt not show the blue box on top left corner)
fig, ax = plt.subplots(figsize=(4,3))
ax.plot(x_axis, psi_1_mean, color='royalblue', linestyle='dashed')
ax.fill_between(x_axis, psi_1_down, psi_1_up, alpha=0.5, edgecolor='royalblue', facecolor='firebrick')
textstr = '\n'.join(('\u03F4' +' ~ 0.3%', '\u03C6 = 0.1 ' + '\u03BC' + 'm'))
props = dict(boxstyle='round', facecolor='royalblue', alpha=0.5)
ax.text(0.05, 0.95, textstr, transform=ax.transAxes, fontsize=12,
        verticalalignment='top', bbox=props)
plt.ylabel(r'$\mathrm{\psi_1}$ [kPa]',**axis_font)
plt.title('Strip biaxial test',**title_font)
plt.xlabel(r'$\mathrm{\lambda_x}$',**axis_font )
#plt.show()
plt.savefig('Psi_1 over lambdaX.pdf', dpi = 300, bbox_inches='tight') # Fig4D
#plt.close()
#%%
## better to comment 64-89 to run the following code (sometimes, plt not show the blue box on top left corner)
fig, ax = plt.subplots(figsize=(4,3))
ax.plot(x_axis, psi_2_mean, color='royalblue', linestyle='dashed')
ax.fill_between(x_axis, psi_2_down, psi_2_up, alpha=0.5, edgecolor='royalblue', facecolor='firebrick')
textstr = '\n'.join(('\u03F4' +' ~ 0.3%', '\u03C6 = 0.1 ' + '\u03BC' + 'm'))
props = dict(boxstyle='round', facecolor='royalblue', alpha=0.5)
ax.text(0.05, 0.95, textstr, transform=ax.transAxes, fontsize=12,
        verticalalignment='top', bbox=props)
plt.ylabel(r'$\mathrm{\psi_2}$ [kPa]',**axis_font)
plt.title('Strip biaxial test',**title_font)
plt.xlabel(r'$\mathrm{\lambda_x}$',**axis_font )
#plt.show()
plt.savefig('Psi_2 over lambdaX.pdf', dpi = 300, bbox_inches='tight')  # Fig4G



################### fig 4B 4E 4H. convergence psi vs dof 
#%%
def read_data(file_name):
    data = pd.read_excel(io = file_name, header=0)
    return data

def clean_data(data):
    data = data.dropna(axis=0) 
    data = data[abs(data['phi_1_unRestrict']) != 0.0000]
    data = data[abs(data['phi_2_unRestrict']) != 0.0000]
    data = data[abs(data['phi_1_unRestrict']) < 30] 
    data = data[abs(data['phi_2_unRestrict']) < 30] 
    data = data[abs(data['phi_1_Restrict']) < 30] 
    data = data[abs(data['phi_2_Restrict']) < 30] 
    #print('filtered by value:',len(data)) 
    return data

train_data = read_data('networks_theta_0.3_scaled_optimized_dri.xlsx')
train_data = clean_data(train_data)
#%%

converge_data = train_data[(train_data['lambdaX'] == 1.25) & (train_data['lambdaY'] == 1.0)]
totalPsi = converge_data['psif_scaled']
percent_nodes = 100*converge_data['percent_inner_node']
fig, ax = plt.subplots(figsize=(4,3))
ax.scatter(percent_nodes, totalPsi, s=10, c= 'royalblue')
plt.title('Convergence of DFN simulation',**title_font)
params = {'mathtext.default': 'regular' }          
plt.rcParams.update(params)
plt.xlabel('$n_{DOF}$',**axis_font)
plt.ylabel(r'$\psi$ [kPa]',**axis_font)
plt.gca().set_xticklabels(['{:.0f}%'.format(x) for x in plt.gca().get_xticks()])
plt.savefig('Energy(psi) over percent inner nodes.pdf', dpi = 300, bbox_inches='tight')  # Fig 4B
plt.show()
#plt.close()

#%%
fig, ax = plt.subplots(figsize=(4,3))
psi_1 = converge_data['phi_1_unRestrict']
ax.scatter(percent_nodes, psi_1, s=10, c= 'royalblue')
plt.title('Convergence of DFN simulation',**title_font)
params = {'mathtext.default': 'regular' }          
plt.rcParams.update(params)
plt.xlabel('$n_{DOF}$',**axis_font)
plt.ylabel(r'$\psi_{1}$ [kPa]',**axis_font)
plt.gca().set_xticklabels(['{:.0f}%'.format(x) for x in plt.gca().get_xticks()])
plt.savefig('Energy(psi_1) over percent inner nodes.pdf', dpi = 300, bbox_inches='tight') # Fig 4E
plt.show()
#plt.close()
#%%
fig, ax = plt.subplots(figsize=(4,3))
psi_2 = converge_data['phi_2_unRestrict']
ax.scatter(percent_nodes, psi_2, s=10, c= 'royalblue')
plt.title('Convergence of DFN simulation',**title_font)
params = {'mathtext.default': 'regular' }          
plt.rcParams.update(params)
plt.xlabel('$n_{DOF}$',**axis_font)
plt.ylabel(r'$\psi_{2}$ [kPa]',**axis_font)
plt.gca().set_xticklabels(['{:.0f}%'.format(x) for x in plt.gca().get_xticks()])
plt.savefig('Energy(psi_2) over percent inner nodes.pdf', dpi = 300, bbox_inches='tight')  #Fig 4H
plt.show()
#plt.close()



################### fig 4C 4F 4I. Dependence of psi on theta and fiber diameter 
#%%
def read_data(file_name):
    data = pd.read_excel(io = file_name, header=0)
    return data

def clean_data(data):
    data = data.dropna(axis=0) 
    data = data[abs(data['phi_1_unRestrict']) != 0.0000]
    data = data[abs(data['phi_2_unRestrict']) != 0.0000]
    data = data[abs(data['phi_1_unRestrict']) < 30] 
    data = data[abs(data['phi_2_unRestrict']) < 30] 
    data = data[abs(data['phi_1_Restrict']) < 30] 
    data = data[abs(data['phi_2_Restrict']) < 30] 
    #print('filtered by value:',len(data)) 
    return data

train_data = read_data('networks_scaled_optimized_dri.xlsx')
train_data = clean_data(train_data)

converge_data = train_data[(train_data['lambdaX'] == 1.25) & (train_data['lambdaY'] == 1.0)]

psi_contour_data = converge_data.copy()
#%%
fig, ax = plt.subplots(figsize=(4,3))
#fig, ax = plt.subplots()
im = ax.scatter(psi_contour_data['percent_volume_fraction'], psi_contour_data['fiber_diameter'], c=psi_contour_data['psif_scaled'], cmap= 'coolwarm')
im.set_clim(psi_contour_data['psif_scaled'].min(),0.5) # 0.2 is better than psi_contour_data['total_psif'].max()
fig.colorbar(im, ax=ax, extend = 'max',label=r'$\psi$ [kPa]')

plt.title(r'$\Psi$ as a function of microstructure',**title_font)
params = {'mathtext.default': 'regular' }   
plt.xlabel('\u03F4 (%)',**axis_font)
plt.ylabel('\u03C6 (' + '\u03BC' +'m)',**axis_font)
plt.savefig('Dependence of energy on theta and fiber diameter.pdf', dpi=300,bbox_inches = "tight") #Fig 4C
plt.show()
#plt.close()
#%%
###phi_1_unRestrict
fig, ax = plt.subplots(figsize=(4,3))
im = ax.scatter(psi_contour_data['percent_volume_fraction'], psi_contour_data['fiber_diameter'], c=psi_contour_data['phi_1_unRestrict'], cmap= 'coolwarm')
im.set_clim(psi_contour_data['phi_1_unRestrict'].min(), 5.0) #3.5 is better than psi_contour_data['phi_1_unRestrict'].max()
fig.colorbar(im, ax=ax, extend = 'max',label=r'$\psi_{1}$ [kPa]')

plt.title(r'$\Psi_1$ as a function of microstructure',**title_font)
params = {'mathtext.default': 'regular' }   
plt.xlabel('\u03F4 (%)',**axis_font)
plt.ylabel('\u03C6 (' + '\u03BC' +'m)',**axis_font)
plt.savefig('Dependence of psi1 on theta and fiber diameter.pdf', dpi=300,bbox_inches = "tight")   #Fig 4F
plt.show()
#plt.close()
#%%
###phi_2_unRestrict
fig, ax = plt.subplots(figsize=(4,3))
im = ax.scatter(psi_contour_data['percent_volume_fraction'], psi_contour_data['fiber_diameter'], c=psi_contour_data['phi_2_unRestrict'], cmap= 'coolwarm')
#im.set_clim(psi_contour_data['phi_2_unRestrict'].min(), psi_contour_data['phi_2_unRestrict'].max())
#im.set_clim(psi_contour_data['phi_2_unRestrict'].min(), 1) 
im.set_clim(-2.5, 0.0) 
fig.colorbar(im, ax=ax, extend = 'max',label=r'$\psi_{2}$ [kPa]')

plt.title(r'$\Psi_2$ as a function of microstructure',**title_font)
params = {'mathtext.default': 'regular' }   
plt.xlabel('\u03F4 (%)',**axis_font)
plt.ylabel('\u03C6 (' + '\u03BC' +'m)',**axis_font)
plt.savefig('Dependence of psi2 on theta and fiber diameter.pdf', dpi=300,bbox_inches = "tight")   #Fig 4I
plt.show()
#plt.close()
