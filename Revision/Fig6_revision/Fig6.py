#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jul  4 14:35:15 2021
@author: cathylengyue
"""

import pandas as pd
import os
import numpy as np
import sys
import tensorflow.keras
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.layers import Input, Dense, Activation
import tensorflow.keras.backend as K
K.clear_session()
from sklearn import preprocessing
from sklearn.preprocessing import StandardScaler
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick

import tensorflow as tf
config = tf.compat.v1.ConfigProto()
config.gpu_options.allow_growth = True
config.gpu_options.per_process_gpu_memory_fraction = 0.2

tf.compat.v1.keras.backend.set_session(tf.compat.v1.Session(config=config))
tf.compat.v1.disable_eager_execution() # need to have this line!!!!


def read_data(file_name):
    data = pd.read_excel(io = file_name, header=0)
    return data

def combine_data(files):
    data_list = []
    for file in files:
        data = read_data(file)
        data_list.append(data)
    combined_data = pd.concat(data_list, ignore_index=True)
    return combined_data

def clean_data(data, dof = True):
    data = data.dropna(axis=0) 
    data = data[abs(data['phi_1_unRestrict']) != 0.0000]
    data = data[abs(data['phi_2_unRestrict']) != 0.0000]
    data = data[abs(data['phi_1_unRestrict']) < 30] 
    data = data[abs(data['phi_2_unRestrict']) < 30] 
    data = data[abs(data['phi_1_Restrict']) < 30] 
    data = data[abs(data['phi_2_Restrict']) < 30] 
    if dof: data = data[abs(data['percent_inner_node']) >= 0.55] 
    #print('filtered by value:',len(data)) 
    return data
     
def X_Y_data(data):
    target = ['phi_1_unRestrict','phi_2_unRestrict']

    indy_variable = [
             'fiber_diameter',
             'percent_volume_fraction',
             'I1',
             'I2']
    
    X = data[indy_variable].values
    Y = data[target].values
    return X,Y

train_data = read_data('networks_scaled_optimized_dri.xlsx') 
train_data = clean_data(train_data, dof = False)
train_X, train_Y = X_Y_data(train_data)
scaler = StandardScaler()  
scaler.fit(train_X)
train_X = scaler.transform(train_X)

a1 = 10**4
a2 = 10**4
def _loss_tensor(y_true, y_pred, x_train):
    
    MAPELoss = K.mean(abs(100*(y_true - y_pred)/y_true), axis=-1)
    MAELoss = K.mean(abs(y_true - y_pred), axis=-1)
    
    MAELoss_psi2 = 100 * K.mean(abs(y_true[:, 1] - y_pred[:, 1]), axis=-1)

    MAPELoss_psi1 = K.mean(abs(100*(y_true[:, 0] - y_pred[:, 0])/y_true[:, 0]), axis=-1)
    MSELoss_psi1 = 1 * K.mean(K.square(y_true[:, 0] - y_pred[:, 0]), axis=-1)
    MAELoss_psi1 = 10 * K.mean(abs(y_true[:, 0] - y_pred[:, 0]), axis=-1)
    
    with tf.GradientTape() as tape:
        term = K.gradients(model.output, model.input)[0]
    
    dPsidI1dI1, dPsidI1dI2, dPsidI2dI1, dPsidI2dI2 = term[2][0],term[3][0],term[2][1],term[3][1]
    offdiag_mean = (dPsidI1dI2 + dPsidI2dI1)/2.0
    PosDefLoss_1 = K.max([0, -dPsidI1dI1])
    
    PosDefLoss_2 = K.max([0, -(dPsidI1dI1 * dPsidI2dI2 - offdiag_mean * offdiag_mean)])
    SymmLoss = abs(dPsidI1dI2 - dPsidI2dI1)
    
    return MSELoss_psi1 + MAPELoss_psi1 + MAELoss_psi1 + MAELoss_psi2 + a1 * (PosDefLoss_1 + PosDefLoss_2) + a2 * SymmLoss

def loss_func(x_train):
        def loss(y_true,y_pred):
            return _loss_tensor(y_true, y_pred, x_train)
        return loss

def convexityScore(y_true, y_pred):
    term = K.gradients(y_pred,model.input)[0] 
    dPsidI1dI1, dPsidI1dI2, dPsidI2dI1, dPsidI2dI2 = term[2][0],term[3][0],term[2][1],term[3][1]
    offdiag_mean = (dPsidI1dI2 + dPsidI2dI1)/2.0
    PosDefLoss_1 = K.max([0, -dPsidI1dI1])
    PosDefLoss_2 = K.max([0, -(dPsidI1dI1 * dPsidI2dI2 - offdiag_mean * offdiag_mean)])
    return PosDefLoss_1 + PosDefLoss_2

def symmetricScore(y_true, y_pred):
    term = K.gradients(y_pred,model.input)[0] 
    dPsidI1dI1, dPsidI1dI2, dPsidI2dI1, dPsidI2dI2 = term[2][0],term[3][0],term[2][1],term[3][1]
    SymmLoss = abs(dPsidI1dI2 - dPsidI2dI1)
    return SymmLoss

def mae_psi1(y_true, y_pred):
        return abs(y_true[:, 0] - y_pred[:, 0])
    
def mae_psi2(y_true, y_pred):
        return abs(y_true[:, 1] - y_pred[:, 1])

def mse_psi1(y_true, y_pred):
        return (y_true[:, 0] - y_pred[:, 0])**2
    
def mse_psi2(y_true, y_pred):
        return (y_true[:, 1] - y_pred[:, 1])**2

def mape_psi1(y_true, y_pred):
        return abs(100*(y_true[:, 0] - y_pred[:, 0])/y_true[:, 0])
    
def mape_psi2(y_true, y_pred):
        return abs(100*(y_true[:, 1] - y_pred[:, 1])/y_true[:, 1])
        
title_font = {'fontname':'Arial', 'size':'15', 'color':'black', 'weight':'normal',
              'verticalalignment':'bottom'} # Bottom vertical alignment for more space
axis_font = {'fontname':'Arial', 'size':'13'}


################### fig 6. FCNN error contour plot lambdaX = lambdaY = 1.15 (microstructure and its true value)
contour_data = read_data('contour_scaled_optimized_dri.xlsx')
contour_data = clean_data(contour_data, dof = False)    
contour_X, contour_Y = X_Y_data(contour_data)
contour_X = scaler.transform(contour_X)
#%%

#model_path = 'scaled_105_105_mix_110010_15100'
model_path = 'scaledall_0_0_mix_1110_00100'
model = tensorflow.keras.models.load_model(model_path, compile=False)
model_loss = loss_func(x_train=model.input)
model.compile(loss = model_loss, optimizer="adam", metrics = ['mean_absolute_percentage_error','mean_absolute_error', 'mean_squared_error', convexityScore,symmetricScore ])
#%%

print(model.evaluate(contour_X[:], contour_Y[:]))
pred_contour_Y = model.predict(contour_X[:])
mape_psi_1_error = abs(100*(contour_Y[:,0] - pred_contour_Y[:,0])/(contour_Y[:,0] + 1e-5))
mae_psi_1_error = abs(contour_Y[:,0] - pred_contour_Y[:,0])
mae_psi_2_error = abs(contour_Y[:,1] - pred_contour_Y[:,1])
largest_indices = np.argsort(-1*mape_psi_1_error)[:10]

contour_data_copy = contour_data.copy()
contour_data_copy["mape_psi_1_error"] = pd.DataFrame(data = mape_psi_1_error) # 因为mean_difference的index是连续的，而contour_data_copy因为之前删除过data，index可能没有对上
contour_data_copy["mae_psi_1_error"] = pd.DataFrame(data = mae_psi_1_error)
contour_data_copy["mae_psi_2_error"] = pd.DataFrame(data = mae_psi_2_error)
uniqueList = set(zip(contour_data_copy.seeds, contour_data_copy.cube_length, contour_data_copy.fiber_diameter))
contour_data_copy["pred_psi_1"] = pd.DataFrame(data = pred_contour_Y[:,0])
contour_data_copy["pred_psi_2"] = pd.DataFrame(data = pred_contour_Y[:,1])
df = contour_data_copy[(contour_data_copy['lambdaX'] == 1.25) & (contour_data_copy['lambdaY'] == 1.25)]

fiber_diam, percent_volume_frac, psi_1_error, psi_2_error, psi_1_true, psi_2_true = [],[],[],[],[],[]
for i,pairs in enumerate(uniqueList):                     
    subdata = df[(df['seeds'] == pairs[0]) & (df['cube_length']== pairs[1]) & (df['fiber_diameter'] == pairs[2])]
    mape_psi_1_err = subdata.mape_psi_1_error.mean()
    mae_psi_1_err = subdata.mae_psi_1_error.mean()
    mae_psi_2_err = subdata.mae_psi_2_error.mean()
    psi_1_ori, psi_2_ori = subdata.phi_1_unRestrict.mean(), subdata.phi_2_unRestrict.mean()
    fiber_diam.append(pairs[2])
    percent_volume_frac.append(subdata.percent_volume_fraction.mean())
    psi_1_error.append(mae_psi_1_err)
    psi_2_error.append(mae_psi_2_err)
    psi_1_true.append(psi_1_ori)
    psi_2_true.append(psi_2_ori)

plt.xlabel('volume fraction \u03F4 [%]',**axis_font)
plt.ylabel('$\mathrm{\psi^{true}_1}$ [kPa]',**axis_font)
plt.title(r'$\mathrm{\psi_1}$ error as a function of $\mathrm{\theta}$ and $\mathrm{\psi^{true}_1}$',**title_font)
points = plt.scatter(np.array(percent_volume_frac), np.array(psi_1_true), c=np.array(psi_1_error),cmap="coolwarm", s=25)
plt.colorbar(points,extend="max",label=r'$\Psi_1$ MAE  (kPa)') # label='relative error %'
plt.clim(vmin=min(psi_1_error), vmax= 1)
plt.savefig('MAE psi1 as psi1 and volumeFrac_nonconvex.pdf', dpi = 300, bbox_inches='tight')  
plt.show()
plt.close()

plt.xlabel('volume fraction \u03F4 [%]',**axis_font)
plt.ylabel('$\mathrm{\psi^{true}_2}$ [kPa]',**axis_font)
plt.title(r'$\mathrm{\psi_2}$ error as a function of $\mathrm{\theta}$ and $\mathrm{\psi^{true}_2}$',**title_font)
points = plt.scatter(np.array(percent_volume_frac), np.array(psi_2_true), c=np.array(psi_2_error),cmap="coolwarm", s=25)
plt.colorbar(points,extend="max",label=r'MAE (kPa)') # label='relative error %'
plt.clim(vmin=min(psi_2_error), vmax= 0.75)
plt.savefig('MAE psi2 as psi2 and volumeFrac_nonconvex.pdf', dpi = 300, bbox_inches='tight') 
plt.show()
plt.close()

plt.ylabel('$\mathrm{\psi^{true}_1}$ [kPa]',**axis_font)
plt.xlabel('fiber diameter \u03C6 [' + '\u03BC' +'m]',**axis_font)
plt.title(r'$\mathrm{\psi_1}$ error as a function of $\mathrm{\varphi}$ and $\mathrm{\psi^{true}_1}$',**title_font)
points = plt.scatter(np.array(fiber_diam), np.array(psi_1_true), c=np.array(psi_1_error),cmap="coolwarm", s=25)
plt.colorbar(points,extend="max",label=r'MAE (kPa)') # label='relative error %'
plt.clim(vmin=min(psi_1_error), vmax= 1)
plt.savefig('MAE psi1 as psi1 and fiberDiam_nonconvex.pdf', dpi = 300, bbox_inches='tight')  
plt.show()
plt.close()

plt.ylabel('$\mathrm{\psi^{true}_2}$ [kPa]',**axis_font)
plt.xlabel('fiber diameter \u03C6 [' + '\u03BC' +'m]',**axis_font)
plt.title(r'$\mathrm{\psi_2}$ error as a function of $\mathrm{\varphi}$ and $\mathrm{\psi^{true}_2}$',**title_font)
points = plt.scatter(np.array(fiber_diam), np.array(psi_2_true), c=np.array(psi_2_error),cmap="coolwarm", s=25)
plt.colorbar(points,extend="max",label=r'MAE (kPa)') # label='relative error %'
plt.clim(vmin=min(psi_2_error), vmax= 0.75)
plt.savefig('MAE psi2 as psi2 and fiberDiam_nonconvex.pdf', dpi = 300, bbox_inches='tight') 
plt.show()
plt.close()


