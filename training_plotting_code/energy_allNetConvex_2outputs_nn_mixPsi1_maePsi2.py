#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 21 17:30:06 2020

@author: cathylengyue
# https://stackoverflow.com/questions/49935778/second-derivative-in-keras
# https://stackoverflow.com/questions/50288258/derivative-in-loss-function-in-keras
# https://stackoverflow.com/questions/56478454/in-tensorflow-2-0-with-eager-execution-how-to-compute-the-gradients-of-a-networ
"""


import pandas as pd
import os
import numpy as np
import sys
import tensorflow.keras
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.layers import Input, Dense, Activation
import tensorflow.keras.backend as K
K.clear_session()
from sklearn import preprocessing
from sklearn.preprocessing import StandardScaler

from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau

import matplotlib.pyplot as plt
import matplotlib.ticker as mtick


import tensorflow as tf
config = tf.compat.v1.ConfigProto()
config.gpu_options.allow_growth = True
config.gpu_options.per_process_gpu_memory_fraction = 0.2
# keras.backend.tensorflow_backend.set_session(tf.compat.v1.Session(config=config))
tf.compat.v1.keras.backend.set_session(tf.compat.v1.Session(config=config))
tf.compat.v1.disable_eager_execution() # need to have this line!!!!
#tf.compat.v1.enable_eager_execution() 
#tf.config.experimental_run_functions_eagerly(True)

def read_data(file_name):
    #data = pd.read_csv(filepath_or_buffer = file_name, header=0)
    data = pd.read_excel(io = file_name, header=0)
    return data

def combine_data(files):
    data_list = []
    for file in files:
        data = read_data(file)
        data_list.append(data)
    combined_data = pd.concat(data_list, ignore_index=True)
    return combined_data

def clean_data(data):
    #print('original:', len(data)) -> original: 117585
    data = data.dropna(axis=0) 
    #print('dropNA:',len(data)) -> dropNA: 117585
    data = data[abs(data['phi_1_unRestrict']) < 30] 
    data = data[abs(data['phi_2_unRestrict']) < 30] 
    data = data[abs(data['phi_1_Restrict']) < 30] 
    data = data[abs(data['phi_2_Restrict']) < 30] 
    #print('filtered by value:',len(data)) -> filtered by value: 117570
    return data
     
train_data = read_data('networks_GP_dri_filtered.xlsx')
train_data = clean_data(train_data)
header = list(train_data)
header = ['seeds', 'cube_length', 'fiber_diameter', 'percent_volume_fraction',
             'total_fibers','inner_nodes','percent_inner_node',
             'lambdaX','lambdaY','lambdaZ',
             'sigmaXX','sigmaYY','sigmaZZ',
             'I1','I2', 'total_psif','dPsidI1','dPsidI2',
             'rho_unRestrict','phi_1_unRestrict','phi_2_unRestrict','rho_Restrict', 'phi_1_Restrict','phi_2_Restrict']

def X_Y_data(data):
    target = ['phi_1_unRestrict','phi_2_unRestrict']
    #target = ['phi_2_unRestrict']
    indy_variable = [#'seeds',
             #'cube_length',
             'fiber_diameter',
             'percent_volume_fraction',
             #'total_fibers',
             # 'inner_nodes',
             # 'percent_inner_node',
             #'lambdaX',
             #'lambdaY',
             'I1',
             'I2',
             # 'total_psif'
             ]
    
    X = data[indy_variable].values
    Y = data[target].values
    return X,Y
    
train_X, train_Y = X_Y_data(train_data)
scaler = StandardScaler()
scaler.fit(train_X)
train_X = scaler.transform(train_X)
#test_X = scaler.transform(test_X)
#train_X = preprocessing.scale(train_X) 
#test_X = preprocessing.scale(test_X) 

a1 = 10000
a2 = 10000
def _loss_tensor(y_true, y_pred, x_train):
    # print(y_true, y_pred,x_train)
    # Tensor("output_layer_target:0", shape=(None, None), dtype=float32) 
    # Tensor("output_layer/BiasAdd:0", shape=(None, 2), dtype=float32) 
    # Tensor("input_layer:0", shape=(None, 4), dtype=float32)
    MAELoss_psi2 = 100 * K.mean(abs(y_true[:, 1] - y_pred[:, 1]), axis=-1)

    MAPELoss_psi1 = K.mean(abs(100*(y_true[:, 0] - y_pred[:, 0])/y_true[:, 0]), axis=-1)
    MSELoss_psi1 = 1 * K.mean(K.square(y_true[:, 0] - y_pred[:, 0]), axis=-1)
    MAELoss_psi1 = 10 * K.mean(abs(y_true[:, 0] - y_pred[:, 0]), axis=-1)
    #print(K.shape(l1)) # Tensor("loss/output_layer_loss/Shape:0", shape=(1,), dtype=int32)
    #print(K.shape(K.abs((y_true - y_pred)/y_true))) # Tensor("loss/output_layer_loss/Shape:0", shape=(2,), dtype=int32)
    #print(y_pred,x_train)
    with tf.GradientTape() as tape:
        term = K.gradients(model.output, model.input)[0]
    #term = K.gradients(y_pred,x_train)[0] 
#    print(term) #Tensor("loss/output_layer_loss/gradients/layer1/MatMul_grad/MatMul:0", shape=(None, 4), dtype=float32)
#    print(K.shape(term))# Tensor("loss/output_layer_loss/Shape:0", shape=(2,), dtype=int32)
    dPsidI1dI1, dPsidI1dI2, dPsidI2dI1, dPsidI2dI2 = term[2][0],term[3][0],term[2][1],term[3][1]
    offdiag_mean = (dPsidI1dI2 + dPsidI2dI1)/2.0
    PosDefLoss_1 = K.max([0, -dPsidI1dI1])
    #print(PosDefLoss_1, K.shape(PosDefLoss_1))
    # Tensor("loss/output_layer_loss/Max:0", shape=(), dtype=float32) Tensor("loss/output_layer_loss/Shape:0", shape=(0,), dtype=int32)
    PosDefLoss_2 = K.max([0, -(dPsidI1dI1 * dPsidI2dI2 - offdiag_mean * offdiag_mean)])
    SymmLoss = abs(dPsidI1dI2 - dPsidI2dI1)
    #return MAPELoss + PosDefLoss_1 + PosDefLoss_2 + SymmLoss
    #return MAPELoss 
    return MSELoss_psi1 + MAPELoss_psi1 + MAELoss_psi1 + MAELoss_psi2 + a1 * (PosDefLoss_1 + PosDefLoss_2) + a2 * SymmLoss

def loss_func(x_train):
        def loss(y_true,y_pred):
            #with tf.GradientTape() as tape:
            #    gradient = K.gradients(model.output, model.input)[0]
            #K.print_tensor(gradient, message='gradient = ')
            return _loss_tensor(y_true, y_pred, x_train)
        return loss

def convexityScore(y_true, y_pred):
    term = K.gradients(y_pred,model.input)[0] 
    dPsidI1dI1, dPsidI1dI2, dPsidI2dI1, dPsidI2dI2 = term[2][0],term[3][0],term[2][1],term[3][1]
    offdiag_mean = (dPsidI1dI2 + dPsidI2dI1)/2.0
    PosDefLoss_1 = K.max([0, -dPsidI1dI1])
    PosDefLoss_2 = K.max([0, -(dPsidI1dI1 * dPsidI2dI2 - offdiag_mean * offdiag_mean)])
    return PosDefLoss_1 + PosDefLoss_2

def symmetricScore(y_true, y_pred):
    term = K.gradients(y_pred,model.input)[0] 
    dPsidI1dI1, dPsidI1dI2, dPsidI2dI1, dPsidI2dI2 = term[2][0],term[3][0],term[2][1],term[3][1]
    SymmLoss = abs(dPsidI1dI2 - dPsidI2dI1)
    return SymmLoss

def mae_psi1(y_true, y_pred):
        return abs(y_true[:, 0] - y_pred[:, 0])
    
def mae_psi2(y_true, y_pred):
        return abs(y_true[:, 1] - y_pred[:, 1])

def mse_psi1(y_true, y_pred):
        return (y_true[:, 0] - y_pred[:, 0])**2
    
def mse_psi2(y_true, y_pred):
        return (y_true[:, 1] - y_pred[:, 1])**2

def mape_psi1(y_true, y_pred):
        return abs(100*(y_true[:, 0] - y_pred[:, 0])/y_true[:, 0])
    
def mape_psi2(y_true, y_pred):
        return abs(100*(y_true[:, 1] - y_pred[:, 1])/y_true[:, 1])

# 154-163 also works
#model = Sequential(
#    [   
#        Input(shape=(4), name = "input_layer"),
#        Dense(8, activation="relu", name="layer1"),
#        Dense(8, activation="relu", name="layer2"),
#        Dense(16, activation="relu",name="layer3"),
#        Dense(2, activation="linear",name="output_layer")
#        
#    ]
#)   
model = Sequential()
model.add(Input(shape=(4,))) # input vector shape
model.add(Dense(units=8))
model.add(Activation('relu'))
model.add(Dense(units=8))
model.add(Activation('relu'))
model.add(Dense(units=16))
model.add(Activation('relu'))

model.add(Dense(units=2))
model.add(Activation('linear'))


model_loss = loss_func(x_train=model.input)
model.compile(loss = model_loss,
              optimizer="adam",
              #metrics = ['mean_absolute_percentage_error',convexityScore,symmetricScore ])
              metrics = [ 'mean_absolute_error', 'mean_absolute_percentage_error', 'mean_squared_error', mae_psi1, mae_psi2, mape_psi1, mape_psi2, mse_psi1, mse_psi2, convexityScore, symmetricScore ])
              #metrics = ['mean_absolute_percentage_error','mean_absolute_error', 'mean_squared_error'])
model_name = 'try_104_104_mixP1_maeP2'

earlyStopping = EarlyStopping(monitor='val_loss', patience=1000, verbose=0, mode='min')
mcp_save = ModelCheckpoint(model_name, save_best_only=True, monitor='val_loss', mode='min')

reduce_lr_loss = ReduceLROnPlateau(monitor='val_loss', patience=1000, verbose=1, epsilon = 1e-5, mode='min')

history = model.fit(train_X, train_Y, epochs = 300, batch_size = 256, callbacks=[mcp_save], validation_split = 0.15)  # model.fit is the steps for training, 300 epochs is large enough to get a working model ## if no need to train, comment this line out
#model.summary()
#print(model.evaluate(train_X[:], train_Y[:], batch_size = 20_000))
#print(model.evaluate(train_X[-int(0.15*len(train_X)):], train_Y[-int(0.15*len(train_X)):], batch_size=20_000))

########## saving parameters to .jason and .h5 file, which will be useful for prediction in testing time
model = tensorflow.keras.models.load_model(model_name, custom_objects={'loss': loss_func(x_train=model.input)},compile=False)
##store model #https://github.com/pplonski/keras2cpp/blob/master/example/mnist_cnn_one_iteration.py
with open('./{}/convexNN_best.json'.format(model_name), 'w') as fout:
   fout.write(model.to_json())
model.save_weights('./{}/convexNN_best.h5'.format(model_name), overwrite=True)
# with open("./convexNN_best.dat", "w") as fin:
#     fin.write("1 4\n")
#     a = train_X[0]
#     for b in a:
#         fin.write(str(b)+' ')

########## prediction steps, and gives the summary of train performance after all training is done
model = tensorflow.keras.models.model_from_json(open('./{}/convexNN_best.json'.format(model_name)).read())
model.load_weights('./{}/convexNN_best.h5'.format(model_name))
model.compile(loss = model_loss, optimizer="adam", metrics = ['mean_absolute_error', 'mean_absolute_percentage_error', 'mean_squared_error', mae_psi1, mae_psi2, mape_psi1, mape_psi2, mse_psi1, mse_psi2,convexityScore,symmetricScore ])
print(model.evaluate(train_X[:int(0.85*len(train_X))], train_Y[:int(0.85*len(train_X))], batch_size = 20_000))
print(model.evaluate(train_X[-int(0.15*len(train_X)):], train_Y[-int(0.15*len(train_X)):], batch_size=20_000))
# get prediction on saved sample
# c++ output should be the same ;)
print('Prediction on saved sample:')
print(str(model.predict(train_X[:5])))

########## if want to write predicted value to an excel sheet
# pred_Y = model.predict(train_X[:])      
# train_data["pred_psi_1"] = pd.DataFrame(data = pred_Y[:,0])
# train_data["pred_psi_2"] = pd.DataFrame(data = pred_Y[:,1])
# train_data.to_excel("./{}/networks_GP_dri_filtered_with_prediction.xlsx".format(model_name))
