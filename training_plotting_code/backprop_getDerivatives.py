#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  9 19:19:46 2020

@author: cathylengyue
"""

import numpy as np
class Layer:
    def __init__(self, weights_matrix, bias_vector, relu_activation = True):
        self.weights_matrix = weights_matrix
        self.bias_vector = bias_vector
        self.relu_activation = relu_activation

    def compute_value(self, x_vector):
        result = np.add(np.dot(self.weights_matrix, x_vector), self.bias_vector)
        if self.relu_activation:
            result = np.maximum(result, 0)
        else: # activation = linear activation
            result = result
        return result

    # d(S_j(x)/dx_i) = w_ij if S_j(x) > 0 else 0
    def compute_value_and_derivative(self, x_vector):
        if not self.relu_activation:
            return (self.compute_value(x_vector), self.weights_matrix)
        temp = np.add(np.dot(self.weights_matrix, x_vector), self.bias_vector)
        temp = np.maximum(temp, 0)
        value = temp
        #print(temp,temp.shape)
        relu_grad = temp > 0
        #print(relu_grad,relu_grad.shape)
        #print(relu_grad.T, relu_grad.T.shape, )
        #pre-multiplying by a diagonal matrix multiplies each row by
        #the corresponding diagonal element
        #(1st row with 1st value, 2nd row with 2nd value, etc...)
        jacobian = np.dot(np.diagflat(relu_grad), self.weights_matrix) # np.dot(relu_grad, self.weights_matrix) or grad_output*relu_grad
        return (value, jacobian)

class Network:
    def __init__(self, layers):
        self.layers = layers

    def compute_value(self, x_vector):
        for l in self.layers:
            x_vector = l.compute_value(x_vector)
        return x_vector

    def compute_value_and_derivative(self, x_vector):
        x_vector, jacobian = self.layers[0].compute_value_and_derivative(x_vector)
        for l in self.layers[1:]:
            x_vector, j = l.compute_value_and_derivative(x_vector)
            jacobian = np.dot(j, jacobian)
        return x_vector, jacobian
    
#first weights
l1w = np.array([        [0.6103268, 0.63040113, -0.10564851, 0.27429223, 0.02780472, -0.61414576, -0.9099403, -0.7970651],
[-0.0090846, 0.0268375, 0.00390461, -0.00442671, -0.00396096, 0.580981, -0.00274125, 0.01717504],
[ 3.62371318e-02, -1.06392436e-01, -1.50170992e-03, 2.06049196e-02, 7.29366875e-05, 5.58265805e-01, 3.09122950e-02, -3.35446522e-02],
[-0.11459526, 0.1749985, 0.01342576, -0.04001463, -0.00891597, -0.38026306, 0.01560239, -0.04387015],
[-0.2784146, -0.4750533, 0.07896841,-0.17616159, -0.0186981, 0.8359189, 0.32542935, 0.5976657],
[1.5990882, 0.06243832, -1.2438962, 1.4088321, -1.0173417, 0.9740659, 1.5016036, 1.2311256],
[-0.69505364, -0.06341636, 1.0164391, -0.6405892, -0.29458335, -0.8223706, -1.1690223, -0.96484876]       ])
l1b = np.array([[0.28466296, 0.1315197, 0.33121827, 0.8432531, -1.2204176, -1.6816764, -0.45449603, 0.14055523]])

l2w = np.array([  [-0.2189135, 0.32336676, 0.88492936, 0.3895156, 0.22209221, 0.9436967, 0.53737324, -0.18806031],
[-0.01629771, -0.35711008, 0.14180416, -0.30132028, 0.6828122, -0.00318094, 0.06049322, 0.01568206],
[-0.14047797, 1.0862912, -0.45605877, 0.7680316, 0.18819128, 0.64377916, -0.6141084, -0.8501088],
[0.42882168, -0.04502463, -0.16311896, -0.1715934, 0.2985447, 0.14594355, 0.49206382, 0.26819003],
[1.0708553, -6.031472, -6.5619497, -0.9541814, -0.19777448, 0.49658525, -0.15194415, -0.10418331],
[2.732365, 1.5177819, 0.5563521, 0.14946003, -6.6752205, 0.53325874, 0.2814753, 1.4746817],
[0.53141516, 1.5219389, -0.5086415, -0.32806808, -1.6411556, 0.23981643, -0.05290747, 0.04793218],
[-1.7147628, -0.18972383, -0.17050663, -0.14186244, -0.44279426, 0.05258239, -0.2366084, -0.15667875]     ])
l2b = np.array([[-0.57484794, -0.9156413, -0.42756727, -0.08676405, -0.3334259, -0.341014, 0.59833586, 0.63540447]])

l3w = np.array([[-8.2363892e-01, -7.0127445e-01, -6.9760650e-02, -4.8558703e-01,
 -8.4195340e-01, -2.1938054e-01, -6.6489980e-02,  2.2202730e-03,
  1.4353720e+00, -9.0591478e-01,  1.0842770e-01, -1.9807331e-03,
  3.7984219e-01, -6.7585268e+00, -2.0210554e-01,  1.9555952e-01],
[0.02575828,  0.25055945, -0.29034233, -0.3523874, 1.2862325,  -0.54483503,
 -0.48198462, -0.20016706, -4.1770153, 0.84125954, -0.3062279,   1.7890415,
  0.2350663,  0.36911508, -0.5232485,  -0.01755466],
[0.61633056,  0.29208302, -0.8375385,  -0.47837242,  0.22401914,  0.1886657,
 -0.29135764, -0.4139204, -1.4428968,  0.7391552,  -0.3116858,  -0.11133725,
 -0.30735153, -0.33126152, -0.5271201,   0.0405436],
[0.04679399, -0.01855525, -0.23112594,  0.09035751, -0.13669442,  0.05735995,
 -0.5167924,  -0.06871819,  0.37000737,  1.3219155,  -0.17515366, -0.16049619,
 -0.27770457,  0.29783368,  0.21057804, -0.37421897],
[8.6078748e-02, -1.1342130e-01, -1.8254725e-02,  1.1339339e-04,
  9.5930248e-01, -2.2495942e-01,  3.8521287e-01, -4.7182238e-01,
  3.9240104e-01,  8.4084645e-02, -7.9637133e-02,  1.1869853e+00,
 -3.5700607e-01,  8.3439595e-01,  2.6606578e-01,  1.3961158e-03],
[0.22126687, -0.660547,   -0.3199338,   0.04576898, -0.5486412,  -0.12076031,
  0.01728615, -0.3185767,   0.01145048,  0.15262373,  0.02535697, -0.7874053,
 -0.37659645, -0.17230769,  0.00605532, -0.3473995],
[0.4881315, 0.19299883,  0.30376375, -0.48493358,  0.04580572, -0.04975784,
 -0.43111098, -0.2295134, 0.36896926, -0.17152232, -0.21244986,  0.01617736,
 -0.5545922,   0.07254446, -0.37405056,  0.15500154],
[-0.14484303, -0.16358702, 0.5439855, 0.08253983, -0.56330925, -0.02747159,
 -0.14708108, -0.3639803,   0.31506103, -0.16243747, -0.87522805, -0.9420533,
 -0.1588457,  -0.4048257,  0.03621674,  0.27558133]])
l3b = np.array([[-0.16905889, 0.36288208, 0.12471187, -0.04884912, 0.02712543, -0.12993649,
 -0.08880013,  0.        , -0.6110902,  -1.2844024,  -0.12040924,  0.0339888,
 -0.10626292, -0.00206956, -0.10028648,  0.1938063]])

l4w = np.array([[0.1485584],
[0.05921952],
[0.06732882],
[0.12564792],
[-0.07454165],
[-0.40288106],
[-0.25352916],
[0.28233606],
[0.026959],
[0.04429575],
[-0.02368176],
[0.07540057],
[-0.4217877],
[-0.02588117],
[-0.45303217],
[0.02256432]])
l4b = np.array([[0.00060505]])
nn = Network([Layer(l1w.T, l1b.T),
              Layer(l2w.T, l2b.T),
              Layer(l3w.T, l3b.T),
              Layer(l4w.T, l4b.T, False)])
#sample_energy = np.array([[-0.224012009494], [-1.247803767], [-1.36799613157], [-1.28274115189], [-0.611989707437], [-1.48878437524], [-1.34339169182]])
#sample_energy = np.array([[-0.656721],	[0.762427],	[-0.336214],	[-1.02476],	[-0.921194],	[-0.448313],	[-0.447689]])
#sample_energy = np.array([[-0.656721],[0.762427],[-0.336214],[-1.02476],	[-0.921194],	[0.499037],	[0.503868]])
#sample_energy = np.array([[-0.656721],[0.762427],[-0.336214],[-1.02476],	[-0.921194],	[-0.0230763],[-0.0205643]])
#sample_energy = np.array([[1.2677],[-0.826712],[-1.13871],[-1.0148],[0.736143],	[-1.48878],	[-1.34339]])
sample_energy = np.array([[1.2677],[-0.826712],[-1.13871],[-1.0148],[0.736143],	[0.270769],	[0.0606458]])
#sample_energy = np.array([[1.2677],[-0.826712],[-1.13871],[-1.0148],	[0.736143],	[1.40436],[1.45317]]) 	
#sample_energy = np.array([[0.846374],[0.751069],	[1.49807],[1.84425],	[1.26797],[2.77038],[3.02699]]) #line52561
#print(sample_energy.shape)
r = nn.compute_value_and_derivative(sample_energy)
print (r)
hessian = np.matmul(r[1].T,r[1])
#print(hessian)

cur_pred = float(r[0])
print(cur_pred)
a = np.copy(sample_energy)
#firstDri = []
#idx = 0
#for val in a:
#    new_val = val + 1e-5
#    new_array = np.copy(a)
#    new_array[idx,0] = new_val
#    new_pred = nn.compute_value_and_derivative(new_array)
#    dri = (new_pred[0] - cur_pred) / 1e-5
#    firstDri.append(float(dri[0]))
#    idx += 1
#firstDri = np.array(firstDri)
#print(firstDri)

#r_first_dri = np.array([9.37695720e-04, -2.72874005e-05, -5.75488878e-06,
#        -7.87939752e-05, -7.00899202e-04,  9.42386106e-03,
#        -8.04184792e-03])
#diff = firstDri - r_first_dri
#print(diff)  

max_input = [1.80288979103, 1.43949251718, 3.21770255047, 1.9463966372, 2.54808178, 2.77038466, 3.02699286] 
min_input = [-1.6018497503, -2.48399500306, -1.59728106567, -1.4164027185, -1.79933555083, -1.48878437524, -1.34339169182] 
inputs_x = []
pred_y = []
for i in range(7):
    range_input = max_input[i] - min_input[i]
    interval = range_input/1000.0
    x, y = [], []
    for val in np.arange(min_input[i], max_input[i] + 0.001, interval):
        x.append(val)
        new_array = np.copy(a)
        new_array[i,0] = val
        new_pred = float(nn.compute_value_and_derivative(new_array)[0])
        y.append(new_pred)
    inputs_x.append(x)
    pred_y.append(y)

    
import matplotlib.pyplot as plt
for i in range(7):
    x, y = inputs_x[i], pred_y[i]
    plt.plot(x,y)
    plt.show()
    
        
#SecondDri = [] # only calculate element on diagnoal line
#idx = 0
#delta = 1e-1
#for val in a:
#    new_val_plus = val + delta
#    new_val_minus = val - delta
#    new_array_plus = np.copy(a)
#    new_array_minus = np.copy(a)
#    new_array_plus[idx,0] = new_val_plus
#    new_array_minus[idx,0] = new_val_minus
#    new_pred_plus = nn.compute_value_and_derivative(new_array_plus)[0]
#    new_pred_minus = nn.compute_value_and_derivative(new_array_minus)[0]
#    #print(new_pred_plus, new_pred_minus, cur_pred)
#    numerator = new_pred_plus + new_pred_minus - 2.0*cur_pred
#    print(numerator)
#    dri = (numerator) / (delta ** 2)
#    SecondDri.append(float(dri[0]))
#    idx += 1
#print(SecondDri)


'''
class Layer:
    #A building block. Each layer is capable of performing two things:
    #- Process input to get output:           output = layer.forward(input)
    #- Propagate gradients through itself:    grad_input = layer.backward(input, grad_output)
    #Some layers also have learnable parameters which they update during layer.backward.
    def __init__(self):
        # Here we can initialize layer parameters (if any) and auxiliary stuff.
        # A dummy layer does nothing
        pass
    
    def forward(self, inputVector):
        # Takes input data of shape [batch, input_units], returns output data [batch, output_units]
        # A dummy layer just returns whatever it gets as input.
        return inputVector
    
    def backward(self, inputVector, grad_output):
        # Performs a backpropagation step through the layer, with respect to the given input.
        # To compute loss gradients w.r.t input, we need to apply chain rule (backprop):
        # d loss / d x  = (d loss / d layer) * (d layer / d x)
        # As we already receive d loss / d layer as input, so, only need to multiply it by d layer / d x.
        # If our layer has parameters (e.g. dense layer), we also need to update them here using d loss / d layer
        # The gradient of a dummy layer is precisely grad_output, but we'll write it more explicitly
        num_units = inputVector.shape[1]
        d_layer_d_input = np.eye(num_units)
        return np.dot(grad_output, d_layer_d_input) # chain rule
    
class ReLU(Layer):
    def __init__(self):
        # ReLU layer simply applies elementwise rectified linear unit to all inputs
        pass
    
    def forward(self, inputVector):
        # Apply elementwise ReLU to [batch, input_units] matrix
        relu_forward = np.maximum(0, inputVector)
        return relu_forward
    
    def backward(self, inputVector, grad_output):
        # Compute gradient of loss w.r.t. ReLU input
        relu_grad = inputVector > 0 # grad = 1 if input > 0
        return grad_output*relu_grad

class Dense(Layer):
    def __init__(self, input_units, output_units, learning_rate=0.1):
        # A dense layer is a layer which performs a learned affine transformation:
        # f(x) = <W*x> + b
        self.learning_rate = learning_rate
        self.weights = np.random.normal(loc=0.0, 
                                        scale = np.sqrt(2/(input_units+output_units)), 
                                        size = (input_units,output_units))
        self.biases = np.zeros(output_units)
        
    def forward(self,input):
        # Perform an affine transformation:
        # f(x) = <W*x> + b
        
        # input shape: [batch, input_units]
        # output shape: [batch, output units]
        
        return np.dot(input,self.weights) + self.biases
    
    def backward(self,input,grad_output):
        # compute d f / d x = d f / d dense * d dense / d x
        # where d dense/ d x = weights transposed
        grad_input = np.dot(grad_output, self.weights.T)
        
        # compute gradient w.r.t. weights and biases
        grad_weights = np.dot(input.T, grad_output)
        grad_biases = grad_output.mean(axis=0)*input.shape[0]
        
        assert grad_weights.shape == self.weights.shape and grad_biases.shape == self.biases.shape
        
        # Here we perform a stochastic gradient descent step. 
        self.weights = self.weights - self.learning_rate * grad_weights
        self.biases = self.biases - self.learning_rate * grad_biases
        
        return grad_input
'''