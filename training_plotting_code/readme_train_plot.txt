'try_104_104_mixP1_maeP2' is the final model used for prediction and figure plotting
'energy_allNetConvex_2outputs_nn_mixPsi1_maePsi2.py' is the file for training (convex function, and predict psi_1 and psi_2)
'energyConvexNN_figurePlots.py' is the file for figures plotting

'backprop_getDerivatives.py' is the file to calculate derivatives with backpropagation
'sigma_training.py' is the old file used for training a NN to do sigma prediction. the code will automatically save the best model


for training:
open energy_allNetConvex_2outputs_nn_mixPsi1_maePsi2.py
line 41-102 is for data loading, preprocessing (remove unusual data and normalize data)
line 104-173 is for convexity restriction and performance evaluation. 
	if don't want to imposing convexity, set a1 = 0 and a2 = 0 in line 104,105; no need to change other code
line 186-210 is for setting the model structure and training parameters
	these code will automatically saving the best model(the one with best validation performance) during training. i.e. you will see a folder with the model_name 'try_104_104_mixP1_maeP2' saved after training
line 212 is for training. model.fit is the line for the whole training process. if don't want to train (eg, just want to try to predict), comment this line out
line 218-222 is saving parameters to .json and .h5 file, which will be useful for prediction in testing time
line 230-238 is the prediction step, and also gives the summary of train performance after all training is done. It is useful for model comparison 