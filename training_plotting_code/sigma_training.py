# -*- coding: utf-8 -*-
"""
Created on Mon Mar  2 23:09:31 2020

@author: skyib
"""

import pandas as pd
import os
import numpy as np

import tensorflow.keras
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.layers import Dense, Activation

from sklearn import preprocessing
from sklearn.preprocessing import StandardScaler

from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau

import matplotlib.pyplot as plt
import matplotlib.ticker as mtick

import tensorflow as tf
config = tf.compat.v1.ConfigProto()
config.gpu_options.allow_growth = True
config.gpu_options.per_process_gpu_memory_fraction = 0.2
# keras.backend.tensorflow_backend.set_session(tf.compat.v1.Session(config=config))
tf.compat.v1.keras.backend.set_session(tf.compat.v1.Session(config=config))

train_path = 'C:\\Users\\Nextran-Adm\\Desktop\\2020RVE\\buganza\\data_energy_0225'
#train_path = 'C:\\Users\\skyib\\Desktop\\YY CS\\buganza\\train_data  -- old 2'

train_files = []
for r, d, f in os.walk(train_path):
    for file in f:
        train_files.append(os.path.join(r, file))


#test_path = 'C:\\Users\\skyib\\Desktop\\YY CS\\buganza\\test_data'
#test_files = []
#for r, d, f in os.walk(test_path):
#    for file in f:
#        test_files.append(os.path.join(r, file))

#header = [seeds,kappa,cube_length,fiber_diam,volume_fraction %, total_fibers,inner_nodes,inner_node %,factorx,factory,sigma00,sigma11,sigma22]


#def read_data(file_name):
#    head_name = np.array(['seeds', 'kappa', 'cube_length', 'fiber_diam', 'volume_fraction %',  'total_fibers', 'inner_nodes', 'inner_node %', 'factorx', 'factory', 'sigma00', 'sigma11', 'sigma22', 'kf = 0.02'])
#    head_name = head_name.reshape(1,14)
#    data = pandas.read_excel(file_name, header=None, names = head_name)
#    
#    return data
#data2 = read_data("network_generation_1.xlsx")

def read_data(file_name):
    data = pd.read_csv(filepath_or_buffer = file_name, header=0)
    return data

#data = read_data("C:\\Users\\skyib\\Desktop\\YY CS\\buganza\\csv_data\\network_generation_1.csv")

def combine_data(files):
    data_list = []
    for file in files:
        data = read_data(file)
        data_list.append(data)
    combined_data = pd.concat(data_list, ignore_index=True)
    return combined_data

def clean_data(data):
#    data = data.drop(['kf = 0.02'], axis=1)
    data = data.dropna(axis=0)
    data = data[abs(data['total_psif'])<10]
#    data = data[abs(data['sigma11'])<100] 
    return data

train_data = combine_data(files = train_files)
#test_data = combine_data(files = test_files)

train_data = clean_data(train_data)
#test_data = clean_data(test_data)
#print(train_data.max())
header = list(train_data)
header = ['seeds',
 'cube_length',
 'fiber_diameter',
 'percent_volume_fraction',
 'total_fibers',
 'inner_nodes',
 'percent_inner_node',
 'lambdaX',
 'lambdaY',
 'I1',
 'I2',
 'total_psif']


def X_Y_data(data):

    
    target = ['sigma00']
#    target = ['sigma11']

    
    indy_variable = [#'seeds',
#             'cube_length',
             'fiber_diameter',
             'percent_volume_fraction',
#             'total_fibers',
#             'inner_nodes',
#             'percent_inner_node',
#             'lambdaX',
#             'lambdaY',
             'I1',
             'I2',
             ]
    
    X = data[indy_variable].values
    Y = data[target].values
    
    return X,Y
    
train_X, train_Y = X_Y_data(train_data)
#test_X, test_Y = X_Y_data(test_data) # https://thispointer.com/find-max-value-its-index-in-numpy-array-numpy-amax/
#np.amax(train_X, axis=0)
scaler = StandardScaler()
scaler.fit(train_X)
train_X = scaler.transform(train_X)
#test_X = scaler.transform(test_X)
#train_X = preprocessing.scale(train_X) 
#test_X = preprocessing.scale(test_X) 


model = Sequential()
model.add(Dense(units=8))
model.add(Activation('relu'))
model.add(Dense(units=8))
model.add(Activation('relu'))
model.add(Dense(units=16))
model.add(Activation('relu'))

#model.add(Dense(units=2, activation='linear'))
model.add(Dense(units=1))
model.add(Activation('linear'))

model.compile(
#        loss='mean_squared_error',
              loss='mean_absolute_percentage_error',
              optimizer='adam',
#              optimizer=keras.optimizers.SGD(lr=0.001, momentum=0.9, nesterov=True),
              metrics = ['mean_absolute_percentage_error'])

model_name = 'energy_8_8_16_0225_best' # you will see a folder with this name after training, which stores the best model

earlyStopping = EarlyStopping(monitor='val_loss', patience=1000, verbose=0, mode='min')
mcp_save = ModelCheckpoint(model_name, save_best_only=True, monitor='val_loss', mode='min') # save the best model while training
reduce_lr_loss = ReduceLROnPlateau(monitor='val_loss', patience=1000, verbose=1, epsilon=1e-5, mode='min')

model.fit(train_X, train_Y, epochs=200, batch_size=256, callbacks=[mcp_save], validation_split=0.15)

#####loss_and_metrics = model.evaluate(test_X, test_Y, batch_size=3600)
#model.save('allinput_try_16-32-32')

model = tensorflow.keras.models.load_model(model_name)
print(model.evaluate(train_X[-int(0.15*len(train_X)):], train_Y[-int(0.15*len(train_X)):], batch_size=20_000)) # help to show the performance
#model.fit(train_X, train_Y, epochs=1_000, callbacks=[mcp_save], batch_size=256, validation_split=0.1) # after 200 epochs in line 160, continue training for another 1000 epochs
#
#print(model.evaluate(train_X[-int(0.1*len(train_X)):], train_Y[-int(0.1*len(train_X)):], batch_size=20_000))
# store model #https://github.com/pplonski/keras2cpp/blob/master/example/mnist_cnn_one_iteration.py
#with open('./my_nn_arch_best.json', 'w') as fout:
#    fout.write(model.to_json())
#model.save_weights('./my_nn_weights_best.h5', overwrite=True)
## store one sample in text file
#with open("./sample_energy_best.dat", "w") as fin:
#    fin.write("1 7\n")
#    a = train_X[0]
#    for b in a:
#        fin.write(str(b)+' ')

# get prediction on saved sample
# c++ output should be the same ;)
#print('Prediction on saved sample:')
#predict_y = model.predict(train_X)
#print(str(model.predict(train_X[:10])))#[[ 0.0014083]]

########################################################################################################### belows are for the plotting
#FIG1 20200330
subscript_map = {
    "0": "₀", "1": "₁", "2": "₂", "3": "₃", "4": "₄", "5": "₅", "6": "₆",
    "7": "₇", "8": "₈", "9": "₉", "a": "ₐ", "b": "♭", "c": "꜀", "d": "ᑯ",
    "e": "ₑ", "f": "բ", "g": "₉", "h": "ₕ", "i": "ᵢ", "j": "ⱼ", "k": "ₖ",
    "l": "ₗ", "m": "ₘ", "n": "ₙ", "o": "ₒ", "p": "ₚ", "q": "૧", "r": "ᵣ",
    "s": "ₛ", "t": "ₜ", "u": "ᵤ", "v": "ᵥ", "w": "w", "x": "ₓ", "y": "ᵧ",
    "z": "₂", "A": "ₐ", "B": "₈", "C": "C", "D": "D", "E": "ₑ", "F": "բ",
    "G": "G", "H": "ₕ", "I": "ᵢ", "J": "ⱼ", "K": "ₖ", "L": "ₗ", "M": "ₘ",
    "N": "ₙ", "O": "ₒ", "P": "ₚ", "Q": "Q", "R": "ᵣ", "S": "ₛ", "T": "ₜ",
    "U": "ᵤ", "V": "ᵥ", "W": "w", "X": "ₓ", "Y": "ᵧ", "Z": "Z", "+": "₊",
    "-": "₋", "=": "₌", "(": "₍", ")": "₎"}
sub_trans = str.maketrans(
    ''.join(subscript_map.keys()),
    ''.join(subscript_map.values()))
#Set the font dictionaries (for plot title and axis titles)
title_font = {'fontname':'Arial', 'size':'15', 'color':'black', 'weight':'normal',
              'verticalalignment':'bottom'} # Bottom vertical alignment for more space
axis_font = {'fontname':'Arial', 'size':'13'}

test_path = 'C:\\Users\\Nextran-Adm\\Desktop\\2020RVE\\buganza\\data_energy_fig1'
test_files = []
for r, d, f in os.walk(test_path):
    for file in f:
        test_files.append(os.path.join(r, file))
test_data = combine_data(files = test_files)
test_data = test_data[test_data['lambdaY'] == 1] # restrict lambday = 1
test_data.reset_index(inplace=True)
test_data = clean_data(test_data)  
test_X, test_Y = X_Y_data(test_data)     
test_X = scaler.transform(test_X) 
df = test_data
unique_cube_len = list(df.cube_length.unique())
I1_list = sorted(list(df.I1.unique()))
pred_y = model.predict(test_X)
from matplotlib.offsetbox import TextArea, DrawingArea, OffsetImage, AnnotationBbox
fig, ax = plt.subplots()
I1_fitParameters, I2_fitParameters =[], [] # quadratic curve fitting parameters for 5 line
avg_x_i1,avg_y_i1,avg_pred_y_i1 = [],[],[] # for verifing derivatives 20200407
gradient_I1,gradient_I1_derive,gradient_I1_taylor = [],[],[] # for verifing derivatives 20200407 # gradient_I1 is using np.gradient
import scipy 
gradient2nd_I1,gradient2nd_I1_deriveHessian,gradient2nd_I1_taylor,gradient2nd_I1_taylor_ = [],[],[],[]
for idx, i in enumerate(unique_cube_len):
    y_mean =  []
    y_std = []
    y_mean_ = []
    grad_I1_derive, firstDri = [],[]
    grad2nd_I1_derive, SecondDri, SecondDri_ = [],[],[]
    for I1 in I1_list:
        index = df[(df['cube_length']==i) & (df['I1'] == I1)].index
        m = df[(df['cube_length']==i) & (df['I1'] == I1)].total_psif.mean()
        s = df[(df['cube_length']==i) & (df['I1'] == I1)].total_psif.std()
        
        y_mean.append(m) #true y
        y_std.append(s)
        
        m_ = pred_y[index, 0].mean()
        y_mean_.append(m_) # predicted
        
        # for verifing 1st derivatives 20200407
        avg_x_i1.append(np.mean(test_X[index], axis=0));avg_y_i1.append(m); avg_pred_y_i1.append(m_) 
        rowData = np.mean(test_X[index], axis=0)
        rowData_ = rowData.reshape((7, 1))
        r = nn.compute_value_and_derivative(rowData_)
        grad_I1_derive.append(r[1][0][5])#r[1][0][5] is for I1, r[1][0][6] is for I2   
        plus_val,minus_val = np.mean(test_X[index], axis=0)[5] + 1e-5, np.mean(test_X[index], axis=0)[5] - 1e-5
        plus_array, minus_array = np.copy(np.mean(test_X[index], axis=0)),np.copy(np.mean(test_X[index], axis=0))
        plus_array[5] = plus_val; minus_array[5] = minus_val
        plus_pred, minus_pred = nn.compute_value_and_derivative(plus_array.reshape((7, 1))), nn.compute_value_and_derivative(minus_array.reshape(7,1))
        dri = (plus_pred[0] - minus_pred[0]) / (2*1e-5)
        firstDri.append(float(dri[0]))
        # for verifing 2nd derivatives 20200407
        hessian = np.matmul(r[1].T,r[1])
        grad2nd_I1_derive.append(hessian.diagonal()[5])
        dri2nd = (plus_pred[0] + minus_pred[0] - 2*r[0]) / (1e-5 * 1e-5)
        SecondDri.append(float(dri2nd[0]))
        plusplus_val,minusminus_val = np.mean(test_X[index], axis=0)[5] + 2*1e-5, np.mean(test_X[index], axis=0)[5] - 2*1e-5
        plusplus_array, minusminus_array = np.copy(np.mean(test_X[index], axis=0)),np.copy(np.mean(test_X[index], axis=0))
        plusplus_array[5] = plusplus_val; minusminus_array[5] = minusminus_val
        plusplus_pred, minusminus_pred = nn.compute_value_and_derivative(plusplus_array.reshape((7, 1))), nn.compute_value_and_derivative(minusminus_array.reshape(7,1))
        dri2nd_ = (-plusplus_pred[0] + 16*plus_pred[0] - 30*r[0] +16*minus_pred[0] - minusminus_pred[0]) / 12*(1e-5 * 1e-5)
        SecondDri_.append(float(dri2nd_[0]))
        
        
    y1 = np.array(y_mean) - 1.96*np.array(y_std)
    y2 = np.array(y_mean) + 1.96*np.array(y_std)
    
    # for verifing 1st derivatives 20200407
    coefficients = np.polyfit(np.array(I1_list), y_mean, 2)
    polynomial = np.poly1d(coefficients) #print(polynomial)
    I1_fitParameters.append(coefficients)
    dydx_grad = np.gradient(y_mean_, np.array(I1_list), edge_order = 2)
    gradient_I1.append(dydx_grad)
    gradient_I1_derive.append(grad_I1_derive)
    gradient_I1_taylor.append(firstDri)
    # for verifing 2nd derivatives 20200407
    spl = scipy.interpolate.splrep(np.array(I1_list),y_mean_,k=2)
    gradient2nd_I1.append( scipy.interpolate.splev(np.array(I1_list),spl,der=2) )
    gradient2nd_I1_deriveHessian.append(grad2nd_I1_derive)
    gradient2nd_I1_taylor.append(SecondDri)
    gradient2nd_I1_taylor_.append(SecondDri_)
    
    plt.plot(np.array(I1_list), y_mean, color = 'royalblue',linestyle='dashed')#, 'r--')
    plt.fill_between(np.array(I1_list), y1, y2, alpha=0.2, edgecolor='cornflowerblue', facecolor='cornflowerblue')
    textstr = '\n'.join(('\u03F4' +' ~ 0.3%', '\u03C6 = 0.1 ' + '\u03BC' + 'm'))
    props = dict(boxstyle='round', facecolor='firebrick', alpha=0.2)
    ax.text(0.05, 0.95, textstr, transform=ax.transAxes, fontsize=12,
        verticalalignment='top', bbox=props)
    plt.plot(np.array(I1_list), y_mean_, color = 'firebrick')
    
    plt.ylabel('Energy',**axis_font)
    plt.xlabel('I1',**axis_font )
    plt.title('Accuracy of FCNN in strip-biaxial loading vs I1',**title_font)
    #plt.hold(True)
#plt.show()
#plt.savefig('Accuracy of FCNN in strip-biaxial loading vs I1.pdf', dpi = 500, bbox_inches='tight')

    
'''
I2_list = sorted(list(df.I2.unique()))
fig, ax = plt.subplots()
avg_x_i2,avg_y_i2,avg_pred_y_i2 = [],[],[] # for verifing derivatives 20200407
for idx, i in enumerate(unique_cube_len):
    y_mean =  []
    y_std = []
    y_mean_ = []
    for I2 in I2_list:
        index = df[(df['cube_length']==i) & (df['I2'] == I2)].index
        m = df[(df['cube_length']==i) & (df['I2'] == I2)].total_psif.mean()
        s = df[(df['cube_length']==i) & (df['I2'] == I2)].total_psif.std()
        
        y_mean.append(m) #true y
        y_std.append(s)
        
        m_ = pred_y[index, 0].mean()
        y_mean_.append(m_) # predicted
        
        avg_x_i2.append(np.mean(test_X[index], axis=0));avg_y_i2.append(m); avg_pred_y_i2.append(m_) # for verifing derivatives 20200407
        
    y1 = np.array(y_mean) - 1.96*np.array(y_std)
    y2 = np.array(y_mean) + 1.96*np.array(y_std)
    
    coefficients = np.polyfit(np.array(I2_list), y_mean, 2)
    polynomial = np.poly1d(coefficients)
    print(polynomial)
    I2_fitParameters.append(coefficients)
    plt.plot(np.array(I2_list), y_mean, color = 'royalblue',linestyle='dashed')#, 'r--')
    plt.fill_between(np.array(I2_list), y1, y2, alpha=0.2, edgecolor='cornflowerblue', facecolor='cornflowerblue')
    textstr = '\n'.join(('\u03F4' +' ~ 0.3%', '\u03C6 = 0.1 ' + '\u03BC' + 'm'))
    props = dict(boxstyle='round', facecolor='firebrick', alpha=0.2)
    ax.text(0.05, 0.95, textstr, transform=ax.transAxes, fontsize=12,
        verticalalignment='top', bbox=props)
    plt.plot(np.array(I2_list), y_mean_, color = 'firebrick')
    
    plt.ylabel('Energy',**axis_font)
    plt.xlabel('I2',**axis_font )
    plt.title('Accuracy of FCNN in strip-biaxial loading vs I2',**title_font)
    #plt.hold(True)
#plt.show()
#plt.savefig('Accuracy of FCNN in strip-biaxial loading vs I2.pdf', dpi = 500, bbox_inches='tight')
'''

    
    





