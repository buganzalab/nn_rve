#include "keras_model.h"

#include <iostream>

using namespace std;
using namespace keras;

// Step 1
// Dump keras model and input sample into text files
// python dump_to_simple_cpp.py -a example/my_nn_arch.json -w example/my_nn_weights.h5 -o example/dumped.nnet
// Step 2
// Use text files in c++ example. To compile:
// g++ keras_model.cc example_main.cc
// To execute:
// a.out

int main() {
  cout << "This is simple example with Keras neural network model loading into C++.\n"
           << "Keras model will be used in C++ for prediction only." << endl;

  DataChunk2D *samples = new DataChunk2D();
  samples->read_from_file("./sample_energy.dat");
  std::cout<<"done readin sample energy\n";
  std::cout<<"print size of data array\n";
  std::cout << samples->get_2d().size() << std::endl;
  std::cout<<"reading nnet file\n";
  KerasModel m("./dumped.nnet", true);
  cout<<"loaded weights\n";
  
  

  // extract a row from the datachunk2D 
  cout<<"samples show values in main\n";
  samples->show_values();
  for(int i=0;i<samples->m_rows;i++){
    cout<<"\n\n\nSAMPLE "<<i<<"\n\n\n";
    // std::vector<float> sample1(samples->m_rows,0.);
    std::vector<float> sample1 = samples->get_row(i);
    //cout<<"print sample 1\n";
    //printvf(sample1);
    //sample1[0]=10;
    //cout<<"print sample 1\n";
    //printvf(sample1);
    //samples->show_values();

    DataChunk *sample = new DataChunkFlat();
    sample->set_data(sample1);
    cout<<"sample (row 1) show values in main\n";
    sample->show_values();

    cout<<"calling keras compute output...\n";
    m.compute_output(sample);
    delete sample;
  }
  delete samples;

  return 0;
}
