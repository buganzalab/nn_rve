#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct  3 23:29:10 2020

@author: cathylengyue
"""

import pandas as pd
import os
import numpy as np
import sys
import tensorflow.keras
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.layers import Input, Dense, Activation
import tensorflow.keras.backend as K
K.clear_session()
from sklearn import preprocessing
from sklearn.preprocessing import StandardScaler

from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau

import matplotlib.pyplot as plt
import matplotlib.ticker as mtick


import tensorflow as tf
config = tf.compat.v1.ConfigProto()
config.gpu_options.allow_growth = True
config.gpu_options.per_process_gpu_memory_fraction = 0.2
# keras.backend.tensorflow_backend.set_session(tf.compat.v1.Session(config=config))
tf.compat.v1.keras.backend.set_session(tf.compat.v1.Session(config=config))
tf.compat.v1.disable_eager_execution() # need to have this line!!!!
#tf.compat.v1.enable_eager_execution() 
#tf.config.experimental_run_functions_eagerly(True)

def read_data(file_name):
    #data = pd.read_csv(filepath_or_buffer = file_name, header=0)
    data = pd.read_excel(io = file_name, header=0)
    return data

def combine_data(files):
    data_list = []
    for file in files:
        data = read_data(file)
        data_list.append(data)
    combined_data = pd.concat(data_list, ignore_index=True)
    return combined_data

def clean_data(data):
    #print('original:', len(data)) -> original: 117585
    data = data.dropna(axis=0) 
    #print('dropNA:',len(data)) -> dropNA: 117585
    data = data[abs(data['phi_1_unRestrict']) != 0.0000]
    data = data[abs(data['phi_2_unRestrict']) != 0.0000]
    data = data[abs(data['phi_1_unRestrict']) < 30] 
    data = data[abs(data['phi_2_unRestrict']) < 30] 
    data = data[abs(data['phi_1_Restrict']) < 30] 
    data = data[abs(data['phi_2_Restrict']) < 30] 
    #print('filtered by value:',len(data)) -> filtered by value: 117570
    return data
     
def X_Y_data(data):
    target = ['phi_1_unRestrict','phi_2_unRestrict']
    #target = ['phi_2_unRestrict']
    indy_variable = [#'seeds',
             #'cube_length',
             'fiber_diameter',
             'percent_volume_fraction',
             #'total_fibers',
             # 'inner_nodes',
             # 'percent_inner_node',
             #'lambdaX',
             #'lambdaY',
             'I1',
             'I2',
             # 'total_psif'
             ]
    
    X = data[indy_variable].values
    Y = data[target].values
    return X,Y
'''
train_data = read_data('networks_GP_dri_filtered.xlsx')
train_data = clean_data(train_data)
header = list(train_data)
header = ['seeds', 'cube_length', 'fiber_diameter', 'percent_volume_fraction',
             'total_fibers','inner_nodes','percent_inner_node',
             'lambdaX','lambdaY','lambdaZ',
             'sigmaXX','sigmaYY','sigmaZZ',
             'I1','I2', 'total_psif','dPsidI1','dPsidI2',
             'rho_unRestrict','phi_1_unRestrict','phi_2_unRestrict','rho_Restrict', 'phi_1_Restrict','phi_2_Restrict']
    
train_X, train_Y = X_Y_data(train_data)
scaler = StandardScaler()
scaler_mean = np.mean(train_X,axis=0) #0.161499,0.45089,3.18844,3.22341
scaler_std = np.std(train_X,axis=0)#0.0879712,0.281284,0.124943,0.164506
scaler.fit(train_X)
train_X = scaler.transform(train_X)
'''




title_font = {'fontname':'Arial', 'size':'15', 'color':'black', 'weight':'normal',
              'verticalalignment':'bottom'} # Bottom vertical alignment for more space
axis_font = {'fontname':'Arial', 'size':'13'}
'''
################### fig 1a. Uncertainty in biaxial test of multiple DFN with the same homogenized microstructure (979 data point, lambda x == lambda y == 1.15)
def read_data(file_name):
    data = pd.read_csv(filepath_or_buffer = file_name, header=0)
    return data
def read_data_xlsx(file_name):
    #data = pd.read_csv(filepath_or_buffer = file_name, header=0)
    data = pd.read_excel(io = file_name, header=0)
    return data
#filename = 'networks.csv'
#biaxial_data = read_data(filename)
#filename = 'biaxial_networks.xlsx'
#biaxial_data = read_data_xlsx(filename)
#df = biaxial_data[biaxial_data['lambdaY'] == 1] # 1.15
x_axis = np.round(np.arange(1,1.251,0.025), decimals=3)
psi_mean = []
psi_std = []
psi_1_mean = []
psi_1_std = []
psi_2_mean = []
psi_2_std = []
for i in x_axis:
    #print(i, df[df['lambdaX']==i]['total_psif'].mean())
    psi_mean.append(df[df['lambdaX']==i]['total_psif'].mean()) 
    psi_std.append(df[df['lambdaX']==i]['total_psif'].std())
    psi_1_mean.append(df[df['lambdaX']==i]['phi_1_unRestrict'].mean()) 
    psi_1_std.append(df[df['lambdaX']==i]['phi_1_unRestrict'].std())
    psi_2_mean.append(df[df['lambdaX']==i]['phi_2_unRestrict'].mean()) 
    psi_2_std.append(df[df['lambdaX']==i]['phi_2_unRestrict'].std())
psi_confidence = 1.96*np.array(psi_std)
psi_down = np.array(psi_mean) - np.array(psi_confidence)
psi_up = np.array(psi_mean) + np.array(psi_confidence)
psi_1_confidence = 1.96*np.array(psi_1_std)
psi_1_down = np.array(psi_1_mean) - np.array(psi_1_confidence)
psi_1_up = np.array(psi_1_mean) + np.array(psi_1_confidence)
psi_2_confidence = 1.96*np.array(psi_2_std)
psi_2_down = np.array(psi_2_mean) - np.array(psi_2_confidence)
psi_2_up = np.array(psi_2_mean) + np.array(psi_2_confidence)

from matplotlib.offsetbox import TextArea, DrawingArea, OffsetImage, AnnotationBbox
import matplotlib.image as mpimg
fig, ax = plt.subplots()

im = mpimg.imread('small_fig.png')
imagebox = OffsetImage(im, zoom=0.7)
ab = AnnotationBbox(imagebox, (1.045, 1.6))
ax.add_artist(ab)

plt.plot(x_axis, psi_mean, color='royalblue', linestyle='dashed')
plt.fill_between(x_axis, psi_down, psi_up, alpha=0.5, edgecolor='royalblue', facecolor='firebrick')
textstr = '\n'.join(('\u03F4' +' ~ 0.3%', '\u03C6 = 0.1 ' + '\u03BC' + 'm'))
props = dict(boxstyle='round', facecolor='royalblue', alpha=0.5)
ax.text(0.05, 0.95, textstr, transform=ax.transAxes, fontsize=12,
        verticalalignment='top', bbox=props)
plt.ylabel(r'$\mathrm{\psi}$',**axis_font)
plt.title('Biaxial test',**title_font)
plt.xlabel(r'$\mathrm{\lambda_x}$',**axis_font )
#plt.show()
plt.savefig('Psi over lambdaX.pdf', dpi = 500, bbox_inches='tight')

plt.plot(x_axis, psi_1_mean, color='royalblue', linestyle='dashed')
plt.fill_between(x_axis, psi_1_down, psi_1_up, alpha=0.5, edgecolor='royalblue', facecolor='firebrick')
textstr = '\n'.join(('\u03F4' +' ~ 0.3%', '\u03C6 = 0.1 ' + '\u03BC' + 'm'))
props = dict(boxstyle='round', facecolor='royalblue', alpha=0.5)
ax.text(0.05, 0.95, textstr, transform=ax.transAxes, fontsize=12,
        verticalalignment='top', bbox=props)
plt.ylabel(r'$\mathrm{\psi_1}$',**axis_font)
plt.title('Biaxial test',**title_font)
plt.xlabel(r'$\mathrm{\lambda_x}$',**axis_font )
#plt.show()
plt.savefig('Psi_1 over lambdaX.pdf', dpi = 500, bbox_inches='tight')

plt.plot(x_axis, psi_2_mean, color='royalblue', linestyle='dashed')
plt.fill_between(x_axis, psi_2_down, psi_2_up, alpha=0.5, edgecolor='royalblue', facecolor='firebrick')
textstr = '\n'.join(('\u03F4' +' ~ 0.3%', '\u03C6 = 0.1 ' + '\u03BC' + 'm'))
props = dict(boxstyle='round', facecolor='royalblue', alpha=0.5)
ax.text(0.05, 0.95, textstr, transform=ax.transAxes, fontsize=12,
        verticalalignment='top', bbox=props)
plt.ylabel(r'$\mathrm{\psi_2}$',**axis_font)
plt.title('Biaxial test',**title_font)
plt.xlabel(r'$\mathrm{\lambda_x}$',**axis_font )
#plt.show()
plt.savefig('Psi_2 over lambdaX.pdf', dpi = 500, bbox_inches='tight')
'''

'''
################### fig 1b. convergence psi vs dof (979 data point, lambda x == lambda y == 1.15)
converge_data = train_data[(train_data['lambdaX'] == 1.15) & (train_data['lambdaY'] == 1.15)]
totalPsi = converge_data['total_psif']
percent_nodes = 100*converge_data['percent_inner_node']

plt.scatter(percent_nodes, totalPsi, s=10, c= 'royalblue')
plt.title('Convergence of DFN simulation',**title_font)
params = {'mathtext.default': 'regular' }          
plt.rcParams.update(params)
plt.xlabel('$n_{DOF}$',**axis_font)
plt.ylabel(r'$\psi$',**axis_font)
plt.gca().set_xticklabels(['{:.0f}%'.format(x) for x in plt.gca().get_xticks()])
plt.savefig('Energy(psi) over percent inner nodes.pdf', dpi = 500, bbox_inches='tight')
#plt.show()
#plt.close()
#totalPsi_mean = totalPsi.mean() # -> 0.12475448363340867
#totalPsi_std = totalPsi.std()   # -> 0.03594653668650981
#mean_percent_error = ( abs(totalPsi - totalPsi_mean) / totalPsi_mean).mean() # -> 0.23424473892061495

psi_1 = converge_data['phi_1_unRestrict']
psi_2 = converge_data['phi_2_unRestrict']

#plt.scatter(percent_nodes, psi_1, s=10, c= 'royalblue')
plt.scatter(percent_nodes, psi_2, s=10, c= 'royalblue')
plt.title('Convergence of DFN simulation',**title_font)
params = {'mathtext.default': 'regular' }          
plt.rcParams.update(params)
plt.xlabel('$n_{DOF}$',**axis_font)
#plt.ylabel(r'$\psi_{1}$',**axis_font)
plt.ylabel(r'$\psi_{2}$',**axis_font)
plt.gca().set_xticklabels(['{:.0f}%'.format(x) for x in plt.gca().get_xticks()])
plt.savefig('Energy(psi_2) over percent inner nodes.pdf', dpi = 500, bbox_inches='tight')
'''



'''
################### fig 1c. Dependence of psi on theta and fiber diameter (979 data point, lambda x == lambda y == 1.15)
psi_contour_data = converge_data.copy()
fig, ax = plt.subplots()
im = ax.scatter(psi_contour_data['percent_volume_fraction'], psi_contour_data['fiber_diameter'], c=psi_contour_data['total_psif'], cmap= 'coolwarm')
#im.set_clim(psi_contour_data['total_psif'].min(), psi_contour_data['total_psif'].max())
im.set_clim(psi_contour_data['total_psif'].min(),0.2)
fig.colorbar(im, ax=ax, extend = 'max',label=r'$\psi$')

plt.title('Mechanical response as a function of microstructure',**title_font)
params = {'mathtext.default': 'regular' }   
plt.xlabel('volume fraction \u03F4 [%]',**axis_font)
plt.ylabel('fiber diameter \u03C6 [' + '\u03BC' +'m]',**axis_font)
plt.savefig('Dependence of energy on theta and fiber diameter.pdf', dpi=500,bbox_inches = "tight")

###phi_1_unRestrict
fig, ax = plt.subplots()
im = ax.scatter(psi_contour_data['percent_volume_fraction'], psi_contour_data['fiber_diameter'], c=psi_contour_data['phi_1_unRestrict'], cmap= 'coolwarm')
#im.set_clim(psi_contour_data['phi_1_unRestrict'].min(), psi_contour_data['phi_1_unRestrict'].max())
im.set_clim(psi_contour_data['phi_1_unRestrict'].min(), 3.5)
fig.colorbar(im, ax=ax, extend = 'max',label=r'$\psi_{1}$')

plt.title('Mechanical response as a function of microstructure',**title_font)
params = {'mathtext.default': 'regular' }   
plt.xlabel('volume fraction \u03F4 [%]',**axis_font)
plt.ylabel('fiber diameter \u03C6 [' + '\u03BC' +'m]',**axis_font)
plt.savefig('Dependence of psi1 on theta and fiber diameter.pdf', dpi=500,bbox_inches = "tight")

###phi_2_unRestrict
fig, ax = plt.subplots()
im = ax.scatter(psi_contour_data['percent_volume_fraction'], psi_contour_data['fiber_diameter'], c=psi_contour_data['phi_2_unRestrict'], cmap= 'coolwarm')
#im.set_clim(psi_contour_data['phi_2_unRestrict'].min(), psi_contour_data['phi_2_unRestrict'].max())
im.set_clim(psi_contour_data['phi_2_unRestrict'].min(), 1)
fig.colorbar(im, ax=ax, extend = 'max',label=r'$\psi_{2}$')

plt.title('Mechanical response as a function of microstructure',**title_font)
params = {'mathtext.default': 'regular' }   
plt.xlabel('volume fraction \u03F4 [%]',**axis_font)
plt.ylabel('fiber diameter \u03C6 [' + '\u03BC' +'m]',**axis_font)
plt.savefig('Dependence of psi2 on theta and fiber diameter.pdf', dpi=500,bbox_inches = "tight")
'''




a1 = 10**4
a2 = 10**4
def _loss_tensor(y_true, y_pred, x_train):
    # print(y_true, y_pred,x_train)
    # Tensor("output_layer_target:0", shape=(None, None), dtype=float32) 
    # Tensor("output_layer/BiasAdd:0", shape=(None, 2), dtype=float32) 
    # Tensor("input_layer:0", shape=(None, 4), dtype=float32)
    MAPELoss = K.mean(abs(100*(y_true - y_pred)/y_true), axis=-1)
    MAELoss = K.mean(abs(y_true - y_pred), axis=-1)
    #print(K.shape(l1)) # Tensor("loss/output_layer_loss/Shape:0", shape=(1,), dtype=int32)
    #print(K.shape(K.abs((y_true - y_pred)/y_true))) # Tensor("loss/output_layer_loss/Shape:0", shape=(2,), dtype=int32)
    #print(y_pred,x_train)
    with tf.GradientTape() as tape:
        term = K.gradients(model.output, model.input)[0]
    #term = K.gradients(y_pred,x_train)[0] 
#    print(term) #Tensor("loss/output_layer_loss/gradients/layer1/MatMul_grad/MatMul:0", shape=(None, 4), dtype=float32)
#    print(K.shape(term))# Tensor("loss/output_layer_loss/Shape:0", shape=(2,), dtype=int32)
    dPsidI1dI1, dPsidI1dI2, dPsidI2dI1, dPsidI2dI2 = term[2][0],term[3][0],term[2][1],term[3][1]
    offdiag_mean = (dPsidI1dI2 + dPsidI2dI1)/2.0
    PosDefLoss_1 = K.max([0, -dPsidI1dI1])
    #print(PosDefLoss_1, K.shape(PosDefLoss_1))
    # Tensor("loss/output_layer_loss/Max:0", shape=(), dtype=float32) Tensor("loss/output_layer_loss/Shape:0", shape=(0,), dtype=int32)
    PosDefLoss_2 = K.max([0, -(dPsidI1dI1 * dPsidI2dI2 - offdiag_mean * offdiag_mean)])
    SymmLoss = abs(dPsidI1dI2 - dPsidI2dI1)
    #return MAPELoss + PosDefLoss_1 + PosDefLoss_2 + SymmLoss
    #return MAPELoss 
    #return MAPELoss + a1 * (PosDefLoss_1 + PosDefLoss_2) + a2 * SymmLoss
    return MAELoss + a1 * (PosDefLoss_1 + PosDefLoss_2) + a2 * SymmLoss

def loss_func(x_train):
        def loss(y_true,y_pred):
            #with tf.GradientTape() as tape:
            #    gradient = K.gradients(model.output, model.input)[0]
            #K.print_tensor(gradient, message='gradient = ')
            return _loss_tensor(y_true, y_pred, x_train)
        return loss

def convexityScore(y_true, y_pred):
    term = K.gradients(y_pred,model.input)[0] 
    dPsidI1dI1, dPsidI1dI2, dPsidI2dI1, dPsidI2dI2 = term[2][0],term[3][0],term[2][1],term[3][1]
    offdiag_mean = (dPsidI1dI2 + dPsidI2dI1)/2.0
    PosDefLoss_1 = K.max([0, -dPsidI1dI1])
    PosDefLoss_2 = K.max([0, -(dPsidI1dI1 * dPsidI2dI2 - offdiag_mean * offdiag_mean)])
    return PosDefLoss_1 + PosDefLoss_2

def symmetricScore(y_true, y_pred):
    term = K.gradients(y_pred,model.input)[0] 
    dPsidI1dI1, dPsidI1dI2, dPsidI2dI1, dPsidI2dI2 = term[2][0],term[3][0],term[2][1],term[3][1]
    SymmLoss = abs(dPsidI1dI2 - dPsidI2dI1)
    return SymmLoss


################### fig 3a. FCNN error biaxial # 5 pairs, each has 20 RVE (lambdaY = 1, strip biaxial)
biaxial_data = read_data('biaxial_networks.xlsx')
biaxial_data = clean_data(biaxial_data)    
biaxial_X, biaxial_Y = X_Y_data(biaxial_data)
biaxial_X = scaler.transform(biaxial_X)

model = Sequential()
model.add(Input(shape=(4,)))
model.add(Dense(units=8))
model.add(Activation('relu'))
model.add(Dense(units=8))
model.add(Activation('relu'))
model.add(Dense(units=16))
model.add(Activation('relu'))
model.add(Dense(units=2))
model.add(Activation('linear'))

model_loss = loss_func(x_train=model.input)
model.compile(loss = model_loss,
              optimizer="adam",
              metrics = ['mean_absolute_percentage_error','mean_absolute_error',convexityScore,symmetricScore ])
model_name = 'try_104_104_1mixP1_maeP2'


model = tensorflow.keras.models.load_model(model_name, custom_objects={'loss': loss_func(x_train=model.input)},compile=False)
model = tensorflow.keras.models.model_from_json(open('./{}/convexNN_best.json'.format(model_name)).read())
model.load_weights('./{}/convexNN_best.h5'.format(model_name))
model_loss = loss_func(x_train=model.input)
model.compile(loss = model_loss, optimizer="adam", metrics = ['mean_absolute_percentage_error','mean_absolute_error', 'mean_squared_error', convexityScore,symmetricScore ])
#print(model.evaluate(biaxial_X[:], biaxial_Y[:], batch_size = 20_000))
#[62.07145690917969, 61.45755, 9.42186e-10, 6.13901e-05]
#print(model.predict(train_X[:10]))
#print(model.predict(biaxial_X[:10]))
pred_biaxial_Y = model.predict(biaxial_X[:])
difference = abs(100*(biaxial_Y - pred_biaxial_Y)/(biaxial_Y + 1e-5))
mean_difference = np.mean(difference, axis = 1)
#print(sorted(mean_difference)[-10:])
#[164.24630482555852, 173.18545641806332, 207.71785302743586, 228.289188865109, 232.30974642773512, 263.0272948219501, 282.6984905908054, 403.11060146489723, 633.8167387840945, 4017.677481799402]
largest_indices = np.argsort(-1*mean_difference)[:10]

biaxial_data_copy = biaxial_data.copy()
biaxial_data_copy["mean_error"] = pd.DataFrame(data = mean_difference) # 因为mean_difference的index是连续的，而contour_data_copy因为之前删除过data，index可能没有对上
biaxial_data_copy["pred_psi_1"] = pd.DataFrame(data = pred_biaxial_Y[:,0])
biaxial_data_copy["pred_psi_2"] = pd.DataFrame(data = pred_biaxial_Y[:,1])
df = biaxial_data_copy[biaxial_data_copy['lambdaY'] == 1]
unique_seed_num = sorted(list(df.seeds.unique()))
unique_cube_len = sorted(list(df.cube_length.unique()))
rgb = ['b', 'g', 'r', 'c', 'y']
x_axis = np.round(np.arange(1,1.251,0.025), decimals=3)
from matplotlib.offsetbox import TextArea, DrawingArea, OffsetImage, AnnotationBbox
import matplotlib.image as mpimg
import matplotlib.colors as mcolors
import matplotlib.cm as cm
mus = [1,2,3,4,5]
colorparams = mus
#colormap = cm.viridis
colormap = cm.coolwarm
normalize = mcolors.Normalize(vmin=np.min(colorparams), vmax=np.max(colorparams))
rgb = [colormap(normalize(mu)) for mu in mus]
fig, ax = plt.subplots()
for idx, i in enumerate(unique_cube_len[:]):
    psi_1_mean = []
    psi_1_std = []
    psi_1_pred_mean = []
    psi_1_pred_std = []
    psi_2_mean = []
    psi_2_std = []
    psi_2_pred_mean = []
    psi_2_pred_std = []
    for j in x_axis:
        psi_1_mean.append(df[(df['lambdaX'] == j) & (df['cube_length'] == i)]['phi_1_unRestrict'].mean()) 
        psi_1_std.append(df[(df['lambdaX'] == j) & (df['cube_length'] == i)]['phi_1_unRestrict'].std())
        psi_1_pred_mean.append(df[(df['lambdaX'] == j) & (df['cube_length'] == i)]['pred_psi_1'].mean()) 
        psi_1_pred_std.append(df[(df['lambdaX'] == j) & (df['cube_length'] == i)]['pred_psi_1'].std())
        psi_2_mean.append(df[(df['lambdaX'] == j) & (df['cube_length'] == i)]['phi_2_unRestrict'].mean()) 
        psi_2_std.append(df[(df['lambdaX'] == j) & (df['cube_length'] == i)]['phi_2_unRestrict'].std())
        psi_2_pred_mean.append(df[(df['lambdaX'] == j) & (df['cube_length'] == i)]['pred_psi_2'].mean()) 
        psi_2_pred_std.append(df[(df['lambdaX'] == j) & (df['cube_length'] == i)]['pred_psi_2'].std())

    psi_1_confidence = 1.96*np.array(psi_1_std)
    psi_1_down = np.array(psi_1_mean) - np.array(psi_1_confidence)
    psi_1_up = np.array(psi_1_mean) + np.array(psi_1_confidence)
    psi_2_confidence = 1.96*np.array(psi_2_std)
    psi_2_down = np.array(psi_2_mean) - np.array(psi_2_confidence)
    psi_2_up = np.array(psi_2_mean) + np.array(psi_2_confidence)

#    plt.plot(x_axis, psi_1_pred_mean, linestyle='-', color = rgb[idx])
#    plt.plot(x_axis, psi_1_mean, color=rgb[idx], linestyle='--')
#    plt.fill_between(x_axis, psi_1_down, psi_1_up, alpha=0.2, edgecolor=rgb[idx], facecolor=rgb[idx])
    plt.plot(x_axis, psi_2_pred_mean, linestyle='-', color = rgb[idx])
    plt.plot(x_axis, psi_2_mean, color=rgb[idx], linestyle='--')
    plt.fill_between(x_axis, psi_2_down, psi_2_up, alpha=0.2, edgecolor=rgb[idx], facecolor=rgb[idx])
textstr = '\n'.join(('\u03F4' +' ~ 0.3%', '\u03C6 = 0.1 ' + '\u03BC' + 'm'))
props = dict(boxstyle='round', facecolor='firebrick', alpha=0.5)
ax.text(0.05, 0.95, textstr, transform=ax.transAxes, fontsize=12, verticalalignment='top', bbox=props)
#plt.ylabel(r'$\mathrm{\psi_1}$ (KJ/$m^{3}$)',**axis_font)
#plt.title('Accuracy of FCNN in strip-biaxial predicting $\mathrm{\psi_1}$',**title_font)
plt.ylabel(r'$\mathrm{\psi_2}$ (KJ/$m^{3}$)',**axis_font)
plt.title('Accuracy of FCNN in strip-biaxial predicting $\mathrm{\psi_2}$',**title_font)
plt.xlabel(r'$\mathrm{\lambda_x}$',**axis_font )
newList = []
for i in unique_seed_num: 
    newList.append(i)
    newList.append(i)
plt.legend(newList, loc='upper right', title = 'Seeds in DFN')
#plt.show()
#plt.savefig('Accuracy of FCNN in strip-biaxial predicting psi_1.pdf', dpi = 500, bbox_inches='tight')
plt.savefig('Accuracy of FCNN in strip-biaxial predicting psi_2.pdf', dpi = 500, bbox_inches='tight')





'''
################### fig 3b. FCNN error contour plot lambdaX = lambdaY = 1.15 equal biaxial
#contour_data = read_data('contour_optimized_dri.xlsx')
#contour_data = clean_data(contour_data)    
#contour_X, contour_Y = X_Y_data(contour_data)
#contour_X = scaler.transform(contour_X)

model = Sequential()
model.add(Input(shape=(4,)))
model.add(Dense(units=8))
model.add(Activation('relu'))
model.add(Dense(units=8))
model.add(Activation('relu'))
model.add(Dense(units=16))
model.add(Activation('relu'))
model.add(Dense(units=2))
model.add(Activation('linear'))

model_loss = loss_func(x_train=model.input)
model.compile(loss = model_loss,
              optimizer="adam",
              metrics = ['mean_absolute_percentage_error',convexityScore,symmetricScore ])
model_name = 'try_104_104_1mixP1_maeP2'

model = tensorflow.keras.models.load_model(model_name, custom_objects={'loss': loss_func(x_train=model.input)},compile=False)
model = tensorflow.keras.models.model_from_json(open('./{}/convexNN_best.json'.format(model_name)).read())
model.load_weights('./{}/convexNN_best.h5'.format(model_name))
model_loss = loss_func(x_train=model.input)
model.compile(loss = model_loss, optimizer="adam", metrics = ['mean_absolute_percentage_error',convexityScore,symmetricScore ])
print(model.evaluate(contour_X[:], contour_Y[:], batch_size = 20_000))
#[65.90325630986909, 65.88286, 4.7143214e-12, 2.0536645e-06]
#print(model.predict(train_X[:10]))
#print(model.predict(contour_X[:10]))
pred_contour_Y = model.predict(contour_X[:])
mape_psi_1_error = abs(100*(contour_Y[:,0] - pred_contour_Y[:,0])/(contour_Y[:,0] + 1e-5))
mae_psi_2_error = abs(contour_Y[:,1] - pred_contour_Y[:,1])
#print(mean_difference.max()) 117804.29725210958
largest_indices = np.argsort(-1*mape_psi_1_error)[:10]

contour_data_copy = contour_data.copy()
contour_data_copy["mape_psi_1_error"] = pd.DataFrame(data = mape_psi_1_error) # 因为mean_difference的index是连续的，而contour_data_copy因为之前删除过data，index可能没有对上
contour_data_copy["mae_psi_2_error"] = pd.DataFrame(data = mae_psi_2_error)
uniqueList = set(zip(contour_data_copy.seeds, contour_data_copy.cube_length, contour_data_copy.fiber_diameter))
contour_data_copy["pred_psi_1"] = pd.DataFrame(data = pred_contour_Y[:,0])
contour_data_copy["pred_psi_2"] = pd.DataFrame(data = pred_contour_Y[:,1])
df = contour_data_copy[(contour_data_copy['lambdaX'] == 1.15) & (contour_data_copy['lambdaY'] == 1.15)]
fiber_diam, percent_volume_frac, psi_1_error, psi_2_error = [],[],[],[]
for i,pairs in enumerate(uniqueList):                     
    subdata = df[(df['seeds'] == pairs[0]) & (df['cube_length']== pairs[1]) & (df['fiber_diameter'] == pairs[2])]
    mape_psi_1_err = subdata.mape_psi_1_error.mean()
    mae_psi_2_err = subdata.mae_psi_2_error.mean()
    fiber_diam.append(pairs[2])
    percent_volume_frac.append(subdata.percent_volume_fraction.mean())
    psi_1_error.append(mape_psi_1_err)
    psi_2_error.append(mae_psi_2_err)
plt.xlabel('volume fraction \u03F4 [%]',**axis_font)
plt.ylabel('fiber diameter \u03C6 [' + '\u03BC' +'m]',**axis_font)

#plt.title('FCNN predicted $\mathrm{\psi_1}$ error as a function of microstructure',**title_font)
#points = plt.scatter(np.array(percent_volume_frac), np.array(fiber_diam), c=np.array(psi_1_error),cmap="coolwarm", s=20)
#plt.colorbar(points,extend="max",label=r'MAPE $\varepsilon$ (%)') # label='relative error %'
#plt.clim(vmin=min(psi_1_error), vmax= 50)
#plt.savefig('MAPE psi1.pdf', dpi = 500, bbox_inches='tight')  

plt.title('FCNN predicted $\mathrm{\psi_2}$ error as a function of microstructure',**title_font)
points = plt.scatter(np.array(percent_volume_frac), np.array(fiber_diam), c=np.array(psi_2_error),cmap="coolwarm", s=20)
plt.colorbar(points,extend="max",label=r'MAE $\varepsilon$') # label='relative error %'
plt.clim(vmin=min(psi_2_error), vmax= 0.75)
plt.savefig('MAE psi2.pdf', dpi = 500, bbox_inches='tight')  
'''

'''
fiber_diam, percent_volume_frac, psi_1_error, psi_2_error, psi_1_true, psi_2_true = [],[],[],[],[],[]
for i,pairs in enumerate(uniqueList):                     
    subdata = df[(df['seeds'] == pairs[0]) & (df['cube_length']== pairs[1]) & (df['fiber_diameter'] == pairs[2])]
    mape_psi_1_err = subdata.mape_psi_1_error.mean()
    mae_psi_2_err = subdata.mae_psi_2_error.mean()
    psi_1_ori, psi_2_ori = subdata.phi_1_unRestrict.mean(), subdata.phi_2_unRestrict.mean()
    fiber_diam.append(pairs[2])
    percent_volume_frac.append(subdata.percent_volume_fraction.mean())
    psi_1_error.append(mape_psi_1_err)
    psi_2_error.append(mae_psi_2_err)
    psi_1_true.append(psi_1_ori)
    psi_2_true.append(psi_2_ori)

#plt.xlabel('volume fraction \u03F4 [%]',**axis_font)
#plt.ylabel('$\mathrm{\psi^{true}_1}$ [KJ/$m^{3}$]',**axis_font)
#plt.title(r'FCNN predicted $\mathrm{\psi_1}$ error as a function of $\mathrm{\theta}$ and $\mathrm{\psi^{true}_1}$',**title_font)
#points = plt.scatter(np.array(percent_volume_frac), np.array(psi_1_true), c=np.array(psi_1_error),cmap="coolwarm", s=25)
#plt.colorbar(points,extend="max",label=r'MAPE $\varepsilon$ (%)') # label='relative error %'
#plt.clim(vmin=min(psi_1_error), vmax= 50)
#plt.savefig('MAPE psi1 as psi1 and volumeFrac.pdf', dpi = 500, bbox_inches='tight')  

#plt.xlabel('volume fraction \u03F4 [%]',**axis_font)
#plt.ylabel('$\mathrm{\psi^{true}_2}$ [KJ/$m^{3}$]',**axis_font)
#plt.title(r'FCNN predicted $\mathrm{\psi_2}$ error as a function of $\mathrm{\theta}$ and $\mathrm{\psi^{true}_2}$',**title_font)
#points = plt.scatter(np.array(percent_volume_frac), np.array(psi_2_true), c=np.array(psi_2_error),cmap="coolwarm", s=25)
#plt.colorbar(points,extend="max",label=r'MAE $\varepsilon$') # label='relative error %'
#plt.clim(vmin=min(psi_2_error), vmax= 0.75)
#plt.savefig('MAE psi2 as psi2 and volumeFrac.pdf', dpi = 500, bbox_inches='tight') 

#plt.ylabel('$\mathrm{\psi^{true}_1}$ [KJ/$m^{3}$]',**axis_font)
#plt.xlabel('fiber diameter \u03C6 [' + '\u03BC' +'m]',**axis_font)
#plt.title(r'FCNN predicted $\mathrm{\psi_1}$ error as a function of $\mathrm{\varphi}$ and $\mathrm{\psi^{true}_1}$',**title_font)
#points = plt.scatter(np.array(fiber_diam), np.array(psi_1_true), c=np.array(psi_1_error),cmap="coolwarm", s=25)
#plt.colorbar(points,extend="max",label=r'MAPE $\varepsilon$ (%)') # label='relative error %'
#plt.clim(vmin=min(psi_1_error), vmax= 50)
#plt.savefig('MAPE psi1 as psi1 and fiberDiam.pdf', dpi = 500, bbox_inches='tight')  


plt.ylabel('$\mathrm{\psi^{true}_2}$ [KJ/$m^{3}$]',**axis_font)
plt.xlabel('fiber diameter \u03C6 [' + '\u03BC' +'m]',**axis_font)
plt.title(r'FCNN predicted $\mathrm{\psi_2}$ error as a function of $\mathrm{\varphi}$ and $\mathrm{\psi^{true}_2}$',**title_font)
points = plt.scatter(np.array(fiber_diam), np.array(psi_2_true), c=np.array(psi_2_error),cmap="coolwarm", s=25)
plt.colorbar(points,extend="max",label=r'MAE $\varepsilon$') # label='relative error %'
plt.clim(vmin=min(psi_2_error), vmax= 0.75)
plt.savefig('MAE psi2 as psi2 and fiberDiam.pdf', dpi = 500, bbox_inches='tight') 

'''