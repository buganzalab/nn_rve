"network_gen_isotropic.py" to generate DFN
"network_equ_xy_energy.py" to solve equilibrium
"contour_energy.py" is the main function to generate DFN and solve equilibrium. in this example, the solved DFN data was used for error contour plot, so it is called contour_energy
then, we need to use "networks_GP.py" to fit GP to get psi_1 and psi_2
