#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 14 20:10:54 2020

@author: cathylengyue
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Jan  7 04:11:06 2020
@author: Nextran-Adm
                          kf = 0.02 line117,199,514, load on XY on 20200107
"""

## Equilibrium of fiber network
# add a parameter 'factor' in solver
import numpy as np
import sys
from numpy import sin, cos, exp, pi
#import timeit
#start = timeit.default_timer()
## Read in the DFN
def read_DFN(filename):
	#filename = 'network500.vtk'
	lines = open(filename,'r').readlines()
	for i in range(len(lines)):
		if lines[i][0:6]=='POINTS':
			aux = lines[i].split(' ')
			n_nodes = int (aux[1])
			nodes_X = np.zeros((n_nodes,3))
			for j in range(n_nodes):
				auxn = lines[i+j+1].split(' ')
				nodes_X[j,0] = float(auxn[0])
				nodes_X[j,1] = float(auxn[1])
				nodes_X[j,2] = float(auxn[2])
		if lines[i][0:5] == 'CELLS':
			aux = lines[i].split(' ')
			n_fibers = int (aux[1])
			fibers = np.zeros((n_fibers,2),dtype=int)
			for j in range(n_fibers):
				auxf = lines[i+j+1].split(' ')
				fibers[j,0] = int(auxf[1])
				fibers[j,1] = int(auxf[2])
			break

	print('nodes, ',n_nodes)
	print('fibers, ',n_fibers)
	return nodes_X,fibers,n_nodes,n_fibers

## Identify the boundaries and fill DOF map
def fillDOFmap(n_nodes,nodes_X):
	bound_xo = []
	bound_xf = []
	bound_yo = []
	bound_yf = []
	bound_zo = []
	bound_zf = []
	DOF_fmap = -1*np.ones((n_nodes,3),dtype=int)
	DOF_imap = []
	tol = 1e-2
	count = 0
	for i in range(n_nodes):
		if nodes_X[i,0]<tol:
			bound_xo.append(i)
		elif nodes_X[i,0]>1.0-tol:
			bound_xf.append(i)
		if nodes_X[i,1]<tol:
			bound_yo.append(i)
		elif nodes_X[i,1]>1.0-tol:
			bound_yf.append(i)
		if nodes_X[i,2]<tol:
			bound_zo.append(i)
		elif nodes_X[i,2]>1.0-tol:
			bound_zf.append(i)
		if (i in bound_xo) or (i in bound_xf) or (i in bound_yo or (i in bound_yf) or (i in bound_zo) or (i in bound_zf)):
			# nothing
			nothing = 0
		else:
			DOF_fmap[i,0] = count
			DOF_fmap[i,1] = count+1
			DOF_fmap[i,2] = count+2
			DOF_imap.append(i*3)
			DOF_imap.append(i*3+1)
			DOF_imap.append(i*3+2)
			count+=3

	n_dof = count
	return n_dof,DOF_fmap,DOF_imap, bound_xo, bound_xf, bound_yo, bound_yf, bound_zo, bound_zf

## Create deformed DFN by first copying the undeformed DFN
def apply_F(F,n_nodes,nodes_X):
	nodes_x = np.zeros((n_nodes,3))
	for i in range(n_nodes):
		nodes_x[i,:] = nodes_X[i,:]
	nodes_u = np.zeros((n_nodes,3))
	for i in range(n_nodes):
		nodes_x[i] = np.dot(F,nodes_X[i])
		nodes_u[i] = nodes_x[i]-nodes_X[i]
	return nodes_u,nodes_x 

## Function to get single fiber residual
#def res_single_fiber(x0,x1,X0,X1):
#	lamdaf = np.linalg.norm(x1-x0)/np.linalg.norm(X1-X0)
#	kf = 0.02 # stiffness of fiber 
#	psif = 0.5*kf*(lamdaf**2-1)**2
#	resf = np.zeros((6))
#	resf[0:3] = -2*(kf/np.linalg.norm(X1-X0)**2)*((x1-x0))*(lamdaf**2-1)
#	resf[3:6] =  2*(kf/np.linalg.norm(X1-X0)**2)*((x1-x0))*(lamdaf**2-1)
#	return resf

## Function to get single fiber residual and tangent
def RK_single_fiber(x0,x1,X0,X1):
	lamdaf = np.linalg.norm(x1-x0)/np.linalg.norm(X1-X0)
	kf = 0.02 # stiffness of fiber 
	psif = 0.5*kf*(lamdaf**2-1)**2
	resf = np.zeros((6))
	tanf = np.zeros((6,6))
	resf[0:3] = -2*(kf/np.linalg.norm(X1-X0)**2)*((x1-x0))*(lamdaf**2-1)
	resf[3:6] =  2*(kf/np.linalg.norm(X1-X0)**2)*((x1-x0))*(lamdaf**2-1)
	tanf[0:3,0:3] =  2*(kf/(np.linalg.norm(X1-X0)**2))*np.eye(3)*(lamdaf**2-1)+4*(kf/(np.linalg.norm(X1-X0)**4))*(np.outer(x1-x0,x1-x0))
	tanf[3:6,3:6] =  2*(kf/(np.linalg.norm(X1-X0)**2))*np.eye(3)*(lamdaf**2-1)+4*(kf/(np.linalg.norm(X1-X0)**4))*(np.outer(x1-x0,x1-x0))
	tanf[0:3,3:6] = -2*(kf/(np.linalg.norm(X1-X0)**2))*np.eye(3)*(lamdaf**2-1)-4*(kf/(np.linalg.norm(X1-X0)**4))*(np.outer(x1-x0,x1-x0))
	tanf[3:6,0:3] = -2*(kf/(np.linalg.norm(X1-X0)**2))*np.eye(3)*(lamdaf**2-1)-4*(kf/(np.linalg.norm(X1-X0)**4))*(np.outer(x1-x0,x1-x0))
	return resf,tanf, psif
	
## Function to calculate the energy of the fibers
#def energy_fibers():
#	energy = 0
#	for i in range(n_fibers):
#		X0 = nodes_X[fibers[i,0]]
#		X1 = nodes_X[fibers[i,1]]
#		x0 = nodes_x[fibers[i,0]]
#		x1 = nodes_x[fibers[i,1]]
#		ei = energy_single_fiber(x0,x1,X0,X1)
#		energy+= ei
#	return energy 

## Function to get residual and tangent of fibers
# NOTE: requires everything to be global variables 
def RK_fibers(n_dof,DOF_fmap,n_nodes,nodes_X,nodes_x,n_fibers,fibers):
	residual = np.zeros((n_dof))
	tangent = np.zeros((n_dof,n_dof))
	ksubs = 0.0000 # stiffness of substrate 
	total_psif = 0.0
	for i in range(n_fibers):
		X0 = nodes_X[fibers[i,0]]
		X1 = nodes_X[fibers[i,1]]
		x0 = nodes_x[fibers[i,0]]
		x1 = nodes_x[fibers[i,1]]
		resi,tani, psif = RK_single_fiber(x0,x1,X0,X1)
		total_psif +=  psif          
		for ni in range(3):
			if DOF_fmap[fibers[i,0],ni]>-1:
				residual[DOF_fmap[fibers[i,0],ni]] += resi[ni] - (x0[ni]-X0[ni])*ksubs
				tangent[DOF_fmap[fibers[i,0],ni],DOF_fmap[fibers[i,0],ni]] -= ksubs
				for nj in range(3):
					if DOF_fmap[fibers[i,0],nj]>-1:
						tangent[DOF_fmap[fibers[i,0],ni],DOF_fmap[fibers[i,0],nj]] += tani[ni,nj]
					if DOF_fmap[fibers[i,1],nj]>-1:
						tangent[DOF_fmap[fibers[i,0],ni],DOF_fmap[fibers[i,1],nj]] += tani[ni,3+nj]
			if DOF_fmap[fibers[i,1],ni]>-1:
				residual[DOF_fmap[fibers[i,1],ni]] += resi[ni+3] - (x1[ni]-X1[ni])*ksubs
				tangent[DOF_fmap[fibers[i,1],ni],DOF_fmap[fibers[i,1],ni]] -= ksubs
				for nj in range(3):
					if DOF_fmap[fibers[i,0],nj]>-1:
						tangent[DOF_fmap[fibers[i,1],ni],DOF_fmap[fibers[i,0],nj]] += tani[ni+3,nj]
					if DOF_fmap[fibers[i,1],nj]>-1:
						tangent[DOF_fmap[fibers[i,1],ni],DOF_fmap[fibers[i,1],nj]] += tani[ni+3,3+nj]
	return residual,tangent,total_psif


## Solver 
def solverXY(n_dof,DOF_fmap,DOF_imap,n_nodes,nodes_X,nodes_x,nodes_u,n_fibers,fibers,bound_xo,bound_xf,bound_yo,bound_yf,bound_zo,bound_zf, factorX = 0.2, factorY = 0.2 ,load_step = 0.2):
	load = 0
	F = np.zeros((3,3))
	while load<1:
		load = load+load_step if load+load_step < 1 else 1 # 0.1
		F[0,0] = 1. + factorX*load 
		F[1,1] = 1. + factorY*load
		F[2,2] = 1./(F[0,0]*F[1,1])      
        
		for i in range(len(bound_xo)):
			nodes_x[bound_xo[i]] = np.dot(F,nodes_X[bound_xo[i]])
		for i in range(len(bound_xf)):
			nodes_x[bound_xf[i]] = np.dot(F,nodes_X[bound_xf[i]])
		for i in range(len(bound_yo)):
			nodes_x[bound_yo[i]] = np.dot(F,nodes_X[bound_yo[i]])
		for i in range(len(bound_yf)):
			nodes_x[bound_yf[i]] = np.dot(F,nodes_X[bound_yf[i]])
		for i in range(len(bound_zo)):
			nodes_x[bound_zo[i]] = np.dot(F,nodes_X[bound_zo[i]])
		for i in range(len(bound_zf)):
			nodes_x[bound_zf[i]] = np.dot(F,nodes_X[bound_zf[i]])
		resnorm = 1.0
		tol = 1e-5
		eps = 1e-5
		iter = 0
		itermax = 200
		while resnorm>tol and iter<itermax:
			residual,tangent,total_psif =  RK_fibers(n_dof,DOF_fmap,n_nodes,nodes_X,nodes_x,n_fibers,fibers)
			resnorm = np.linalg.norm(residual)
			delta = np.linalg.solve(tangent,residual)
			# update solution 
			for i in range(n_dof):
				dof_n = DOF_imap[i]//3
				dof_c = DOF_imap[i]%3
				nodes_x[dof_n,dof_c] -= delta[i]
			iter+=1
		#print('load, ', load,', iter, ',iter,'resnorm, ',resnorm)

	# update displacements
	for i in range(n_nodes):
		nodes_u[i] = nodes_x[i]-nodes_X[i]
		
	return total_psif

## calculate stress
def calc_sigma(n_nodes,nodes_x,nodes_X,n_fibers,fibers,bound_xo, bound_xf, bound_yo, bound_yf, bound_zo, bound_zf):
	sigma = np.zeros((3,3))
	forces = np.zeros((n_nodes,3))
	for i in range(n_fibers):  
		node0 = fibers[i,0]
		node1 = fibers[i,1]
		X0 = nodes_X[fibers[i,0]]       
		X1 = nodes_X[fibers[i,1]]                          
		x0 = nodes_x[fibers[i,0]] 
		x1 = nodes_x[fibers[i,1]]
		lamdaf = np.linalg.norm(x1-x0)/np.linalg.norm(X1-X0)
		kf = 0.02 
		Ef = 0.5*(lamdaf**2-1)
		
		fvec = 2*(kf/(np.linalg.norm(X1-X0)**2))*((x1-x0))*(lamdaf**2-1) # changed to + on 20191228

		forces[node0]+= -fvec
		forces[node1]+= fvec
		if ((node0 in bound_xo) or (node0 in bound_xf) or (node0 in bound_yo) or (node0 in bound_yf) or (node0 in bound_zo) or (node0 in bound_zf)):
	    	#print(fvec)
			for ci in range(3):
				for cj in range(3):
					sigma[ci][cj] -= x0[ci] * fvec[cj]
		if ((node1 in bound_xo) or (node1 in bound_xf) or (node1 in bound_yo) or (node1 in bound_yf) or (node1 in bound_zo) or (node1 in bound_zf)):
			#sum_force += fvec
			#print(fvec)
			for ci in range(3):
				for cj in range(3):
					sigma[ci][cj] += x1[ci] * fvec[cj]
	#print(nodes_summed)
	#print('sum forces')
	#print(sum_force)
	return sigma

## Save deformed
def savefile(namevtk,n_nodes,nodes_x,n_fibers,fibers,nodes_u):
	#namevtk = 'network500_sim1.vtk'
	outfile = open(namevtk,'w')
	outfile.write("# vtk DataFile Version 2.0\n pig membrane patch\nASCII\nDATASET UNSTRUCTURED_GRID\n")
	outfile.write("POINTS %i double\n"%(n_nodes))
	for i in range(n_nodes):
		outfile.write('%f %f %f\n'%(nodes_x[i,0],nodes_x[i,1],nodes_x[i,2]))
	outfile.write("CELLS %i %i\n"%(n_fibers,n_fibers*3))
	for i in range(n_fibers):
		outfile.write('%i %i %i\n'%(2,fibers[i,0],fibers[i,1]))
	outfile.write("CELL_TYPES %i\n"%n_fibers)
	for i in range(n_fibers):
		outfile.write('3\n')
	outfile.write("POINT_DATA %i\nVECTORS displacement float\n"%(n_nodes))
	for i in range(n_nodes):
		outfile.write('%f %f %f\n'%(nodes_u[i,0],nodes_u[i,1],nodes_u[i,2]))
	outfile.close();

def fiber_volume_fraction(fiber_cnt, fiber_diameter, avg_fiber_length, fiber_scale, cube_length):
    true_avg_fiber_length =  avg_fiber_length * fiber_scale
    Area_f = (fiber_diameter / 2) ** 2 * pi 
    single_fiber_volume = Area_f * true_avg_fiber_length
    total_fiber_volume = single_fiber_volume * fiber_cnt
    RVE_volume = cube_length ** 3
    volume_fraction_theta = total_fiber_volume / RVE_volume
    return volume_fraction_theta

def get_fiberCnt_fiberPos(filename):
    lines = open(filename,'r').readlines()
    for i in range(len(lines)):
        if lines[i][0:6]=='POINTS':
            aux = lines[i].split(' ')
            n_nodes = int (aux[1])
            nodes_X = np.zeros((n_nodes,3))
            for j in range(n_nodes):
                auxn = lines[i+j+1].split(' ')
                nodes_X[j,0] = float(auxn[0])
                nodes_X[j,1] = float(auxn[1])
                nodes_X[j,2] = float(auxn[2])
        if lines[i][0:5] == 'CELLS':
            aux = lines[i].split(' ')
            n_fibers = int (aux[1])
            fibers = np.zeros((n_fibers,2),dtype=int)
            for j in range(n_fibers):
                auxf = lines[i+j+1].split(' ')
                fibers[j,0] = int(auxf[1])
                fibers[j,1] = int(auxf[2])
            break
    #return nodes_X,fibers,n_nodes,n_fibers
    return fibers, nodes_X

def get_avg_fiber_length(fibers, nodes_X): # in unit box
    fiber_cnt = len(fibers)
    fiber_length = []
    for i in range(fiber_cnt):  
        node0 = fibers[i,0]
        node1 = fibers[i,1]
        X0 = nodes_X[fibers[i,0]] 
        X1 = nodes_X[fibers[i,1]] 
        #print(X0, X1)                         
        #print(X1 - X0)
        distance = np.linalg.norm(X1 - X0) # axis = 1
        fiber_length.append(distance)
        avg_fiber_length = np.average(np.array(fiber_length))
    return avg_fiber_length



#namevtk = 'seeds270_cube39.22_fiber0.29_isotropic_kf0.02.vtk'
#seeds = int(namevtk[5:namevtk.find('_')])
#cube_start = namevtk.find('cube')+4
#cube_end = namevtk.find('_',cube_start)
#cube_length = float(namevtk[cube_start:cube_end])
#fiber_start = namevtk.find('fiber')+5
#fiber_end = namevtk.find('_',fiber_start)
#fiber_diameter = float(namevtk[fiber_start:fiber_end])
#
#fibers, nodes_X = get_fiberCnt_fiberPos(namevtk)
#unitBox_avg_fiber_length = get_avg_fiber_length(fibers, nodes_X)
#fiber_scale = cube_length / 1.0
#volume_fraction = fiber_volume_fraction( len(fibers), fiber_diameter, unitBox_avg_fiber_length,fiber_scale, cube_length )
#percent_volume_fraction = volume_fraction * 100
#print('cubic_side_length(um):',cube_length,';  Fiber_diameter(um):',fiber_diameter)
#print('avg_fiber_length(um):', unitBox_avg_fiber_length * fiber_scale, ';  Volume_fraction:', volume_fraction * 100,'%')
#
#nodes_X,fibers,n_nodes,n_fibers = read_DFN(namevtk)
#n_dof,DOF_fmap,DOF_imap,bound_xo, bound_xf, bound_yo, bound_yf, bound_zo, bound_zf = fillDOFmap(n_nodes,nodes_X)
##print('n_dof',n_dof)
#F = np.eye(3)
#nodes_u,nodes_x  = apply_F(F,n_nodes,nodes_X)
#for step in np.arange(0.1,1.01,0.01):
#    try:
#        I1,I2,total_psif = solverXY(n_dof,DOF_fmap,DOF_imap,n_nodes,nodes_X,nodes_x,nodes_u,n_fibers,fibers,bound_xo,bound_xf,bound_yo,bound_yf,bound_zo,bound_zf,factorX = 0.0,factorY = 0.2,load_step = step)
#        break
#    except:
#        continue
#total_fibers = len(fibers)
#inner_node = total_fibers - seeds
#percent_inner_node = inner_node / float(seeds)
#results = [seeds, cube_length, fiber_diameter, percent_volume_fraction, total_fibers, inner_node, percent_inner_node, I1,I2, total_psif]
#print(results)
## item in results will be saved in excel sheet