#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 14 21:23:33 2020

@author: cathylengyue
"""

import numpy as np
import random as rand
import network_equ_xy_energy 
#import network_gen_isotropic 
import openpyxl 
import collections
import timeit
import glob
# [(50,10),(100,14),(150,17),(200,20),(250,21.7),(300,23),(350,25),(400,27),(450,29),(500,30)]
def generate_networks_fig1():
    pairs = [(250,21.7),(300,23.5),(350,25),(400,27),(450,29)]
    for seeds, cube_length in pairs:
        valid_cnt = 0
        kappa = 0.0
        fiber_diameter = 0.1
        while valid_cnt < 20: # generate 10 network
            namevtk = 'seeds{}_cube{}_fiber{}_isotropic_kf0.02_{}.vtk'.format(seeds,cube_length,fiber_diameter, valid_cnt)
            percent_volume_fraction, total_fibers = network_gen_isotropic.main(seeds, namevtk, kappa, cube_length, fiber_diameter)
            inner_node = total_fibers - seeds
            percent_inner_node = (inner_node / seeds) * 100
            if 0.05 <= percent_volume_fraction <= 1.00:
                valid_cnt += 1
    return 

def checking_energy_fig1():

    filename = 'networks.xlsx'
    wb = openpyxl.load_workbook(filename)
    ws = wb.active
    lastrow = ws.max_row
    currow = lastrow + 1
    
    networks = glob.glob('generated_networks/seeds*.vtk')
    #networks = glob.glob('seeds*.vtk')
    for file in networks:
        slash_pos = file.find('/')
        namevtk = file[slash_pos + 1:]
        print(namevtk)
        seeds = int(namevtk[5:namevtk.find('_')])
        cube_start = namevtk.find('cube')+4
        cube_end = namevtk.find('_',cube_start)
        cube_length = float(namevtk[cube_start:cube_end])
        length = cube_length 
        fiber_start = namevtk.find('fiber')+5
        fiber_end = namevtk.find('_',fiber_start)
        fiber_diameter = float(namevtk[fiber_start:fiber_end])
        namevtk = 'generated_networks/' + namevtk 
        fibers, nodes_X = network_equ_xy_energy.get_fiberCnt_fiberPos(namevtk)
        unitBox_avg_fiber_length = network_equ_xy_energy.get_avg_fiber_length(fibers, nodes_X)
        fiber_scale = cube_length / 1.0
        volume_fraction = network_equ_xy_energy.fiber_volume_fraction( len(fibers), fiber_diameter, unitBox_avg_fiber_length,fiber_scale, cube_length )
        percent_volume_fraction = volume_fraction * 100
        print('cubic_side_length(um):',cube_length,';  Fiber_diameter(um):',fiber_diameter)
        print('avg_fiber_length(um):', unitBox_avg_fiber_length * fiber_scale, ';  Volume_fraction:', volume_fraction * 100,'%')
        
        nodes_X,fibers,n_nodes,n_fibers = network_equ_xy_energy.read_DFN(namevtk)
        n_dof,DOF_fmap,DOF_imap,bound_xo, bound_xf, bound_yo, bound_yf, bound_zo, bound_zf = network_equ_xy_energy.fillDOFmap(n_nodes,nodes_X)
        F = np.eye(3)
        nodes_u,nodes_x  = network_equ_xy_energy.apply_F(F,n_nodes,nodes_X)
        
        total_fibers = len(fibers)
        inner_node = total_fibers - seeds
        percent_inner_node = inner_node / float(seeds)
        #kf = 0.02
        for factorX in np.arange(0.0, 0.251, 0.025):
            for factorY in np.arange(0.0, 0.251, 0.025):
                #if factorX == 0.0 and factorY == 0.0: continue
                for step in np.arange(0.1,1.01,0.01):
                    try:
                        F00 = 1. + factorX
                        F11 = 1. + factorY
                        F22 = 1./(F00*F11)   
                        I1 =  F00 * F00 + F11 * F11 + F22 * F22
                        I2 =  F00**2 * F11**2 + F00**2 * F22**2 + F11**2 * F22**2 
                        #print(1+factorX,1+factorY, I1,I2)
                        total_psif = network_equ_xy_energy.solverXY(n_dof,DOF_fmap,DOF_imap,n_nodes,nodes_X,nodes_x,nodes_u,n_fibers,fibers,bound_xo,bound_xf,bound_yo,bound_yf,bound_zo,bound_zf,factorX,factorY,load_step = step)
                        break
                    except:
                        continue
                sigma = network_equ_xy_energy.calc_sigma(n_nodes,nodes_x,nodes_X,n_fibers,fibers,bound_xo, bound_xf, bound_yo, bound_yf, bound_zo, bound_zf)
                print('sigma: ',sigma)
                if str(sigma[0][0]) != 'nan':
                    sigma00 = (sigma[0][0] / (length ** 2)) * 1000
                    sigma11 = (sigma[1][1] / (length ** 2)) * 1000
                    sigma22 = (sigma[2][2] / (length ** 2)) * 1000
                lambdaX, lambdaY, lambdaZ = F00,F11,F22
                results = [seeds, length, fiber_diameter, percent_volume_fraction, total_fibers, inner_node, percent_inner_node, lambdaX, lambdaY, lambdaZ, sigma00, sigma11, sigma22, I1,I2, total_psif]#, rho_, phi_1_, phi_2_]
                for i,item in enumerate(results):
                    ws.cell(column = i + 1, row = currow, value=item)
                currow += 1
                wb.save(filename)
        wb.save(filename)

start = timeit.default_timer()
#generate_networks_fig1()
checking_energy_fig1()
end = timeit.default_timer()
print(end - start)

filename = 'networks.xlsx'
import xlrd
import csv
with xlrd.open_workbook(filename) as wb:
    sh = wb.sheet_by_index(0)  # or wb.sheet_by_name('name_of_the_sheet_here')
    with open('networks.csv', 'wb') as f:
    #with open('networks.csv', 'w', newline="") as f:   # open('a_file.csv', 'w', newline="") for python 3
        c = csv.writer(f)
        for r in range(sh.nrows):
            c.writerow(sh.row_values(r))        
        
        
