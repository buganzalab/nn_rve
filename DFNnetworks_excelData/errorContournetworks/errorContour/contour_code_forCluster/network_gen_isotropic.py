
#!/usr/bin/python
# -*- coding: utf-8 -*-
## Generate a collagen network


import numpy as np
import random as rand
from numpy import sin, cos, exp, pi
import os
#import timeit
#start = timeit.default_timer()
'''
unit : um  #
rve side length  um
fiber diameter  um

commentedout line152 noVon-misses & changed line146 phi = np.arccos(1-2*rand.uniform(0,1))   on 20200107
'''
def main(n_fibers, namevtk, kappa = 0, cube_length = 10.0, fiber_diameter = 0.1): # in real um
	# overal definition of the system parameters and aux functions
	#n_fibers = 160
    box = [[0.0,1.0],[0.0,1.0],[0.0,1.0]]
    tfake = 0.0
    fiber_scale = cube_length / 1.0
    fiber_diam = fiber_diameter/ fiber_scale
    dt = fiber_diam 
    
    def fiber_volume_fraction(fiber_cnt, fiber_diameter, avg_fiber_length, fiber_scale, cube_length):
        true_avg_fiber_length =  avg_fiber_length * fiber_scale
        Area_f = (fiber_diameter / 2) ** 2 * pi 
        single_fiber_volume = Area_f * true_avg_fiber_length
        total_fiber_volume = single_fiber_volume * fiber_cnt
        RVE_volume = cube_length ** 3
        volume_fraction_theta = total_fiber_volume / RVE_volume
        return volume_fraction_theta

    def get_fiberCnt_fiberPos(filename):
        lines = open(filename,'r').readlines()
        for i in range(len(lines)):
            if lines[i][0:6]=='POINTS':
                aux = lines[i].split(' ')
                n_nodes = int (aux[1])
                nodes_X = np.zeros((n_nodes,3))
                for j in range(n_nodes):
                    auxn = lines[i+j+1].split(' ')
                    nodes_X[j,0] = float(auxn[0])
                    nodes_X[j,1] = float(auxn[1])
                    nodes_X[j,2] = float(auxn[2])
            if lines[i][0:5] == 'CELLS':
                aux = lines[i].split(' ')
                n_fibers = int (aux[1])
                fibers = np.zeros((n_fibers,2),dtype=int)
                for j in range(n_fibers):
                    auxf = lines[i+j+1].split(' ')
                    fibers[j,0] = int(auxf[1])
                    fibers[j,1] = int(auxf[2])
                break
        #return nodes_X,fibers,n_nodes,n_fibers
        return fibers, nodes_X

    def get_avg_fiber_length(fibers, nodes_X): # in unit box
        fiber_cnt = len(fibers)
        fiber_length = []
        for i in range(fiber_cnt):  
            node0 = fibers[i,0]
            node1 = fibers[i,1]
            X0 = nodes_X[fibers[i,0]] 
            X1 = nodes_X[fibers[i,1]] 
            #print(X0, X1)                         
            #print(X1 - X0)
            distance = np.linalg.norm(X1 - X0) # axis = 1
            fiber_length.append(distance)
            avg_fiber_length = np.average(np.array(fiber_length))
        return avg_fiber_length

	## function to check if a point x is inside a box defined by corner points p0, p1
    def inside(p0,p1,x):
        inx = (p0[0]<=x[0] and x[0]<=p1[0]) or (p0[0]>=x[0] and x[0]>=p1[0])
        iny = (p0[1]<=x[1] and x[1]<=p1[1]) or (p0[1]>=x[1] and x[1]>=p1[1])
        inz = (p0[2]<=x[2] and x[2]<=p1[2]) or (p0[2]>=x[2] and x[2]>=p1[2])
        return inx and iny and inz

	## function to get the distance between point x and a line defined by p0 and p1
    def point_line_dist(p0,p1,x):
        ## get the parameter at which they intersect
        s = np.dot(x-p0,p1-p0)/np.dot(p1-p0,p1-p0)
        ## get the distance
        dvec = x- (p0+(p1-p0)*s)
        d2 = np.linalg.norm(dvec)
        return [s,d2]

    # each fiber is characterized by initial and final point
    # and it is generated based on an initial seed and orientation
    # the orientation is based on two angles in spherical coordinates
    seed_pts = np.zeros((n_fibers,3))
    orientations = np.zeros((n_fibers,3))
    fibers = np.zeros((n_fibers,6))

    ## distribution functions 
    def vonMisesFisher_density_u(u,   kappa,a0):
        # Returns the value of the probability density function of the von Mises distribution,
        # also called the Fisher distribution when we are in 3D
        # The probability density function is evaluated at the vector r
        # The function is parameterized by the scalar concentration parameter kappa, and the 
        # preffered fiber direction a0  
        #norm_cons = kappa/(4.*pi*np.sinh(kappa))
        norm_cons = kappa/(4.*pi*np.sinh(kappa)) if kappa != 0 else 0.0795774715459
        #print(kappa, norm_cons) #-> 1e-10 0.0795774715459
        return norm_cons*exp(kappa*np.dot(u,a0))

#	def vonMisesFisher_density(theta, phi,   kappa, theta0, phi0):
#		# Returns the value of the probability density function of the von Mises distribution,
#		# also called the Fisher distribution when we are in 3D
#		# The probability density function is evaluated at the vector r
#		# The function is parameterized by the scalar concentration parameter kappa, and the 
#		# preffered fiber direction a0 
#		a0 = np.array([np.sin(theta0)*np.cos(phi0),np.sin(theta0)*np.sin(phi0),np.cos(theta0)])
#		u = np.array([np.sin(theta)*np.cos(phi),np.sin(theta)*np.sin(phi),np.cos(theta)])
#		norm_cons = kappa/(4.*pi*np.sinh(kappa))
#		return norm_cons*exp(kappa*np.dot(u,a0))

	## sampling from the distribution 
    def sample_vonMises(kappa, theta0, phi0):
        n_samples = 0
        while n_samples == 0:
            theta = np.pi*rand.uniform(0,1)
            phi = np.pi*rand.uniform(-1,1)
            a0 = np.array([np.sin(theta0)*np.cos(phi0),np.sin(theta0)*np.sin(phi0),np.cos(theta0)])
            u = np.array([np.sin(theta)*np.cos(phi),np.sin(theta)*np.sin(phi),np.cos(theta)])
            if np.dot(a0,u)>0:
                r = vonMisesFisher_density_u(u,kappa,a0)
            else:
                r = vonMisesFisher_density_u(-u,kappa,a0)
            aux = np.random.rand()
            if aux<r:
                n_samples+=1
        return theta,phi 


	# fill in the seed and orientations and create the two points of the fiber
    for i in range(n_fibers):
        seed_pts[i,0] = rand.uniform(0,1)
        seed_pts[i,1] = rand.uniform(0,1)
        seed_pts[i,2] = rand.uniform(0,1)
        ## change from uniform to different von mises distributions
        phi = np.arccos(1-2*rand.uniform(0,1))
        theta = np.pi*rand.uniform(-1,1)
        theta0 = pi/2  # fibers align in x if theta0 = pi/2    if theta0 = 0, fibers align in z
        phi0 = 0.
        #kappa = 0.   #original = 10.
        #print(kappa)
        #theta,phi = sample_vonMises(kappa, theta0, phi0)
        orientations[i,0] = np.sin(phi)*np.cos(theta)
        orientations[i,1] = np.sin(phi)*np.sin(theta)
        orientations[i,2] = np.cos(phi)
        fibers[i,0] = seed_pts[i,0] + dt*orientations[i,0]
        fibers[i,1] = seed_pts[i,1] + dt*orientations[i,1]
        fibers[i,2] = seed_pts[i,2] + dt*orientations[i,2]
        fibers[i,3] = seed_pts[i,0] - dt*orientations[i,0]
        fibers[i,4] = seed_pts[i,1] - dt*orientations[i,1]
        fibers[i,5] = seed_pts[i,2] - dt*orientations[i,2]

    #return orientations 
    # grow in a while loop forever but break when fibers cannot grow anymore
    growing_plus = [1]*n_fibers
    growing_minus = [1]*n_fibers
    # store the intersectoins between fibers, '-1' is intersection against the boundary
    intersections = -1*np.ones((n_fibers,2),dtype=int)
    # inverse map of interesction
    intersections2a = np.zeros((n_fibers),dtype=int)
    intersections2b = -1*np.ones((n_fibers,10),dtype=int)
    intersections2c = -1*np.ones((n_fibers,10))
    n_nodes = 0
    n_bound = 0
    while tfake<1e5:
        # increase length of growing fibers by one step
        for i in range(n_fibers):
            # check if it is growing and if it is then grow
            if growing_plus[i]==1:
                fibers[i,0] = fibers[i,0] + dt*orientations[i,0]
                fibers[i,1] = fibers[i,1] + dt*orientations[i,1]
                fibers[i,2] = fibers[i,2] + dt*orientations[i,2]
				# update whether or not should continue growing
				# frist check if it reached the end of the domain
                if fibers[i,0]<0 or fibers[i,0]>1 or fibers[i,1]<0 or fibers[i,1]>1 or fibers[i,2]<0 or fibers[i,2]>1:
                    growing_plus[i] = 0
                    n_bound+=1
                else:
					# then compare to all other fibers and see if it hits
                    for j in range(n_fibers):
                        if i==j:continue
                        #if np.linalg.norm(np.cross((fibers[i,0:3]-seed_pts[j,:]),orientations[j,:]))<1e-3 and inside(fibers[j,0:3],fibers[j,3:],fibers[i,0:3]):
                        p0 = fibers[j,0:3]
                        p1 = fibers[j,3:]
                        x = fibers[i,0:3]
                        [s,d] = point_line_dist(p0,p1,x)
                        if s>=0 and s<=1.0 and d<=fiber_diam :
                            growing_plus[i] = 0
                            n_nodes+=1
                            # store the intersection
                            intersections[i,0] = j
							# inverse map of intersection
							# current number of intersections for this fiber
                            auxi = intersections2a[j]
                            intersections2b[j,auxi] = i*2
                            intersections2c[j,auxi] = s
                            intersections2a[j] = auxi+1
                            break
            if growing_minus[i]==1:
                fibers[i,3] = fibers[i,3] - dt*orientations[i,0]
                fibers[i,4] = fibers[i,4] - dt*orientations[i,1]
                fibers[i,5] = fibers[i,5] - dt*orientations[i,2]
                if fibers[i,3]<0 or fibers[i,3]>1 or fibers[i,4]<0 or fibers[i,4]>1 or fibers[i,5]<0 or fibers[i,5]>1:
                    growing_minus[i] = 0
                    n_bound+=1
                else:
                    for j in range(n_fibers):
                        if i==j:continue
                        #if np.linalg.norm(np.cross((fibers[i,3:]-seed_pts[j,:]),orientations[j,:]))<1e-2 and inside(fibers[j,0:3],fibers[j,3:],fibers[i,3:]):
                        p0 = fibers[j,0:3]
                        p1 = fibers[j,3:]
                        x = fibers[i,3:]
                        [s,d] = point_line_dist(p0,p1,x)
                        if s>=0 and s<=1.0 and d<=fiber_diam :
                            growing_minus[i] = 0
                            n_nodes +=1
                            # store the intersection
                            intersections[i,1] = j
                            auxi = intersections2a[j]
                            intersections2b[j,auxi] = i*2+1
                            intersections2c[j,auxi] = s
                            intersections2a[j] = auxi+1
                            break
        if sum(growing_plus)+sum(growing_minus)<1:
            break
	#print('n_nodes')
	#print(n_nodes)
	#print('n_bound')
	#print(n_bound)

	## Add fibers to account for the intersections
	# initial network has each node belonging to a single fiber, in reality, each
	# intersection of a fiber with another should lead to a split of the fiber
	# into new fibers, in other words, each fiber is defined by two nodes
	# intersections introduce additional nodes but no additional fibers
	# here I take the intersections and update fiber connectivity
    new_n_fibers = n_fibers+n_nodes
    new_fibers = np.zeros((new_n_fibers,2),dtype=int)
	# first do a copy-deep-copy step
    for i in range(n_fibers):
        new_fibers[i,0] = i*2
        new_fibers[i,1] = i*2+1

	# adjust based on the intersections,
	# first order the intersections
    count = 0
    for i in range(n_fibers):
		# sort the intersections based on 's'
        auxi = intersections2a[i]
        if auxi>0:
            #print('sorting')
            auxintersect = np.array([intersections2b[i,:auxi],intersections2c[i,:auxi]])
            #print(auxintersect)
            auxintersect = auxintersect[:,auxintersect[1,:].argsort()]
            #print(auxintersect)
            ## loop and split the fiber into mutiple fibers, the first split is
            new_fibers[i,1] =  int(auxintersect[0,0])
            new_fibers[n_fibers+count,0] = int(auxintersect[0,0])
            # the rest of the splits are all new fibers
            for j in range(1,auxi):
                new_fibers[n_fibers+count,1] = int(auxintersect[0,j])
                new_fibers[n_fibers+count+1,0] = int(auxintersect[0,j])
                count+=1
            # the last split is
            new_fibers[n_fibers+count,1] = 2*i+1
            count+=1


    #print('n_seeds',n_fibers)
    #print('n_nodes',n_nodes)
    #print('new_n_fibers',new_n_fibers)
    #print('count',count)
    #print('total_fibers',(count+n_fibers))

    
	# SAVE the intersection as VTK file
	# you need Paraview to visualize

	#namevtk = 'network160kappa10.vtk'
    outfile = open(namevtk,'w')
    outfile.write("# vtk DataFile Version 2.0\n pig membrane patch\nASCII\nDATASET UNSTRUCTURED_GRID\n")
    outfile.write("POINTS %i double\n"%(n_fibers*2))
    for i in range(n_fibers):
        outfile.write('%f %f %f\n'%(fibers[i,0],fibers[i,1],fibers[i,2]))
        outfile.write('%f %f %f\n'%(fibers[i,3],fibers[i,4],fibers[i,5]))
    outfile.write("CELLS %i %i\n"%(new_n_fibers,new_n_fibers*3))
    for i in range(new_n_fibers):
        outfile.write('%i %i %i\n'%(2,new_fibers[i,0],new_fibers[i,1]))
    outfile.write("CELL_TYPES %i\n"%new_n_fibers)
    for i in range(new_n_fibers):
        outfile.write('3\n')
    outfile.write("POINT_DATA %i\nSCALARS node float 1\nLOOKUP_TABLE default\n"%(n_fibers*2))
    for i in range(n_fibers):
        if fibers[i,0]-fiber_diam<0.0 or fibers[i,0]+fiber_diam>1.0 or fibers[i,1]-fiber_diam<0.0 or fibers[i,1]+fiber_diam>1.0 or fibers[i,2]-fiber_diam<0.0 or fibers[i,2]+fiber_diam>1.0:
            outfile.write('%f\n'%0.0)
        else:
            outfile.write('%f\n'%1.0)
        if fibers[i,3]-fiber_diam<0.0 or fibers[i,3]+fiber_diam>1.0 or fibers[i,4]-fiber_diam<0.0 or fibers[i,4]+fiber_diam>1.0 or fibers[i,5]-fiber_diam<0.0 or fibers[i,5]+fiber_diam>1.0:
            outfile.write('%f\n'%0.0)
        else:
            outfile.write('%f\n'%1.0)
    outfile.close();
    
    total_fibers = count+n_fibers
    fibers, nodes_X = get_fiberCnt_fiberPos(namevtk)
    unitBox_avg_fiber_length = get_avg_fiber_length(fibers, nodes_X)
    volume_fraction = fiber_volume_fraction( len(fibers), fiber_diameter, unitBox_avg_fiber_length,fiber_scale, cube_length )
    print('cubic_side_length(um):',cube_length,';  Fiber_diameter(um):',fiber_diameter)
    print('avg_fiber_length(um):', unitBox_avg_fiber_length * fiber_scale, ';  Volume_fraction:', volume_fraction * 100,'%')
    percent_volume_fraction = volume_fraction * 100
    if not 0.05 <= percent_volume_fraction <= 1.00:
        os.remove(namevtk)
        #print("FileRemoved:", namevtk, percent_volume_fraction)
    return percent_volume_fraction, total_fibers

#namevtk = 'network400_kappa0_cube25_fiber0.1.vtk'
#n_fibers = 400
#main(n_fibers, namevtk, kappa = 0, cube_length = 25.0, fiber_diameter = 0.1)

#namevtk = 'network400_pre.vtk'
#outfile = open(namevtk,'w')
#outfile.write("# vtk DataFile Version 2.0\n pig membrane patch\nASCII\nDATASET UNSTRUCTURED_GRID\n")
#outfile.write("POINTS %i double\n"%(n_fibers*2))
#for i in range(n_fibers):
#	outfile.write('%f %f %f\n'%(fibers[i,0],fibers[i,1],fibers[i,2]))
#	outfile.write('%f %f %f\n'%(fibers[i,3],fibers[i,4],fibers[i,5]))
#outfile.write("CELLS %i %i\n"%(n_fibers,n_fibers*3))
#for i in range(n_fibers):
#	outfile.write('%i %i %i\n'%(2,i*2,i*2+1))
#outfile.write("CELL_TYPES %i\n"%n_fibers)
#for i in range(n_fibers):
#	outfile.write('3\n')
## outfile.write("POINT_DATA %i\nSCALARS node float 1\nLOOKUP_TABLE default\n"%(n_fibers*2))
## for i in range(n_fibers):
## 	if fibers[i,0]-fiber_diam<0.0 or fibers[i,0]+fiber_diam>1.0 or fibers[i,1]-fiber_diam<0.0 or fibers[i,1]+fiber_diam>1.0 or fibers[i,2]-fiber_diam<0.0 or fibers[i,2]+fiber_diam>1.0:
## 		outfile.write('%f\n'%0.0)
## 	else:
## 		outfile.write('%f\n'%1.0)
## 	if fibers[i,3]-fiber_diam<0.0 or fibers[i,3]+fiber_diam>1.0 or fibers[i,4]-fiber_diam<0.0 or fibers[i,4]+fiber_diam>1.0 or fibers[i,5]-fiber_diam<0.0 or fibers[i,5]+fiber_diam>1.0:
## 		outfile.write('%f\n'%0.0)
## 	else:
## 		outfile.write('%f\n'%1.0)
#outfile.close();

#stop = timeit.default_timer()
#print('Time: ', stop - start) 

