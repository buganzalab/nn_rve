import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.tri as tri
import openpyxl 
import time
import glob
import sys
from scipy.optimize import fsolve, root
import scipy.optimize as optimize
import GPy
import os

def read_data(file_name):
    #data = pd.read_csv(filepath_or_buffer = file_name, header=0)
    #data = pd.read_excel("./energyData/"+ file_name, header=0)
    data = pd.read_excel(io = file_name, header=0)
    return data

def GP_fitting(): # every network (120 line) evaluate once
    data = pd.DataFrame()
    for f in glob.glob("contour_unscaled.xlsx"):
        df = pd.read_excel(f)
        data = data.append(df,ignore_index=True)
    data = data.drop_duplicates()
    data = data.dropna()
    header = ['seeds', 'cube_length', 'fiber_diameter', 'percent_volume_fraction',
             'total_fibers','inner_nodes','percent_inner_node',
             'lambdaX','lambdaY','lambdaZ',
             'sigmaXX','sigmaYY','sigmaZZ',
             'I1','I2', 'total_psif']
    unique_percent_volume_fraction = list(data.percent_volume_fraction.unique())
    uniqueList = set(zip(data.fiber_diameter, data.percent_volume_fraction))
    print('length of unique_percent_volume_fraction, uniquePairs(diameter, volumefraction):', len(unique_percent_volume_fraction), len(uniqueList))
    #n_networks = len(data) // 120
    data['psif_scaled'] = ''
    data['dPsidI1'] = ''
    data['dPsidI2'] = ''
    #if unique_percent_volume_fraction != n_networks: print('warning: unique_cube_len != len(data) // 120:',unique_percent_volume_fraction,n_networks)
    #dPsidI1_list, dPsidI2_list = [], []
    for i,pairs in enumerate(uniqueList):                     
        subdata = data[(data['fiber_diameter'] == pairs[0]) & (data['percent_volume_fraction']== pairs[1])]
        indexrange = subdata.index
        indexMin, indexMax = indexrange[0],indexrange[-1]
        if len(indexrange) > 120: 
            print('WARNING: Error might occur')
            print('indexMin, indexMax:',indexMin, indexMax)
        X = subdata[['I1','I2']].values
        Z_NH_unscaled = subdata[['total_psif']].values
        cube_length = subdata[['cube_length']].values[0]
        Z_NH = Z_NH_unscaled/cube_length/cube_length*1000
        X_mean = np.mean(X, axis=0)
        X_std = np.std(X, axis=0)
        X_scaled = (X - X_mean) / X_std
        # and regress between X_s and Z_NH
        k1 = GPy.kern.RBF(input_dim=2, ARD=True)
        try:
            model_NH = GPy.models.GPRegression(X_scaled, Z_NH, k1)
            model_NH.optimize()
            Psi, var = model_NH.predict(X_scaled)
            #prediction of the gradient of the GP
            #ad_Psi, dvar = model_NH.predictive_gradients((Xp - X_m) / X_s)
            grad_Psi, dvar = model_NH.predict_jacobian(X_scaled)
            dPsidI1 = grad_Psi[:,0,0]/X_std[0]
            dPsidI2 = grad_Psi[:,1,0]/X_std[1]
        	#dPsidI1_list = np.append(dPsidI1_list, dPsidI1)
        	#dPsidI2_list = np.append(dPsidI2_list, dPsidI2)
            data.loc[indexrange,['psif_scaled']] = Z_NH
            data.loc[indexrange,['dPsidI1']] = dPsidI1
            data.loc[indexrange,['dPsidI2']] = dPsidI2
        except np.linalg.LinAlgError:
                print('WARNING: numpy.linalg.LinAlgError: not positive definite, even with jitter.')
                pass
        
    data.to_excel("contour_scaled_dri.xlsx",index = False) 
    return 


def get_derivatives():
    data = read_data('contour_scaled_dri.xlsx') # need the excel file from GP_fitting() . need to uncomment the line52 data.to_excel("networks_update_dri.xlsx",index = False) 
    #data = data.dropna()
    header = ['seeds', 'cube_length', 'fiber_diameter', 'percent_volume_fraction',
             'total_fibers','inner_nodes','percent_inner_node',
             'lambdaX','lambdaY','lambdaZ',
             'sigmaXX','sigmaYY','sigmaZZ',
             'I1','I2', 
             'total_psif','psif_scaled','dPsidI1','dPsidI2']
    
    rho_Restrict, phi_1_Restrict, phi_2_Restrict = [],[],[]
    rho_unRestrict, phi_1_unRestrict, phi_2_unRestrict = [],[],[]

    for index, row in data.iterrows():
        lambdaX,lambdaY,lambdaZ = row['lambdaX'],row['lambdaY'],row['lambdaZ']
        sigmaXX,sigmaYY,sigmaZZ = row['sigmaXX'],row['sigmaYY'],row['sigmaZZ']
        dPsidI1,dPsidI2 = row['dPsidI1'],row['dPsidI2']
        
        # check positive definite with hessian matrix
        A = np.array([[6,-4*(lambdaX**2 + lambdaY**2 + lambdaZ**2),4*(lambdaX**-2 + lambdaY**-2 + lambdaZ**-2)],
                      [-4*(lambdaX**2 + lambdaY**2 + lambdaZ**2) ,8*(lambdaX**4 + lambdaY**4 + lambdaZ**4),-24],
                      [4*(lambdaX**-2 + lambdaY**-2 + lambdaZ**-2), -24, 8*(lambdaX**-4 + lambdaY**-4 + lambdaZ**-4)]])
        try:
            np.linalg.cholesky(A)
            #print("row: ", index, "is positive definite.")
        except np.linalg.LinAlgError:
            #print("row: ", index, "is not positive definite.")
            pass
        
        # solving with scipy.optimize
        def func(x):
            rho, phi_1, phi_2 = x[0], x[1], x[2]
            eq1 = -rho + 2 * phi_1 * lambdaX ** 2 - 2 * phi_2 * lambdaX ** (-2) - sigmaXX
            eq2 = -rho + 2 * phi_1 * lambdaY ** 2 - 2 * phi_2 * lambdaY ** (-2) - sigmaYY
            eq3 = -rho + 2 * phi_1 * lambdaZ ** 2 - 2 * phi_2 * lambdaZ ** (-2) - sigmaZZ
            eq4 = phi_1 - dPsidI1
            eq5 = phi_2 - dPsidI2
            return eq1 ** 2 + eq2 ** 2 + eq3 ** 2 + eq4 ** 2 + eq5 ** 2
        
        cons = [{'type':'ineq', 'fun': lambda x:  x[1]},  # 写不出phi_1和I1的关系。reponse surface. I1 changes-> I2 also change. taylor ? 10-5/10-1
                {'type':'ineq', 'fun': lambda x:  x[2]}]
        #https://stackoverflow.com/questions/35432478/scipy-minimize-constrained-function
        
        initial_guess = [0., 0., 0.]
        result_unrestrict = optimize.minimize(func, initial_guess) # without constraints
        if result_unrestrict.success:
            fitted_params = result_unrestrict.x
            #print(fitted_params)
            #print(result_restrict.fun)# the min value found by minimization # https://docs.scipy.org/doc/scipy-0.18.1/reference/generated/scipy.optimize.OptimizeResult.html#scipy.optimize.OptimizeResult
            rho, phi_1, phi_2 = fitted_params[0],fitted_params[1],fitted_params[2]
            rho_unRestrict.append(rho); phi_1_unRestrict.append(phi_1); phi_2_unRestrict.append(phi_2)
        else:
            print("unable to have a good solution for unrestrict for row:", index, row)
            #print(-sys.maxsize) = -9223372036854775807
            rho_unRestrict.append(-sys.maxsize); phi_1_unRestrict.append(-sys.maxsize); phi_2_unRestrict.append(-sys.maxsize)
            #pass

        result_restrict = optimize.minimize(func, initial_guess, constraints=cons)
        if result_restrict.success:
            fitted_params = result_restrict.x
            #print(fitted_params)
            #print(result_restrict.fun)# the min value found by minimization # https://docs.scipy.org/doc/scipy-0.18.1/reference/generated/scipy.optimize.OptimizeResult.html#scipy.optimize.OptimizeResult
            rho, phi_1, phi_2 = fitted_params[0],fitted_params[1],fitted_params[2]
            rho_Restrict.append(rho); phi_1_Restrict.append(phi_1); phi_2_Restrict.append(phi_2)
        else:
            #print("unable to have a good solution for restrict res for row:", index, row)
            #print(-sys.maxsize) = -9223372036854775807
            rho_Restrict.append(-sys.maxsize); phi_1_Restrict.append(-sys.maxsize); phi_2_Restrict.append(-sys.maxsize)
            #pass
        
    data['rho_unRestrict'] = pd.Series(rho_unRestrict)
    data['phi_1_unRestrict'] = pd.Series(phi_1_unRestrict)
    data['phi_2_unRestrict'] = pd.Series(phi_2_unRestrict)

    data['rho_Restrict'] = pd.Series(rho_Restrict)
    data['phi_1_Restrict'] = pd.Series(phi_1_Restrict)
    data['phi_2_Restrict'] = pd.Series(phi_2_Restrict)
    
    data.to_excel("contour_scaled_optimized_dri.xlsx",index = False) 
    return data

start_time = time.time()
#GP_fitting() 
data = get_derivatives()  
end_time = time.time()
print("--- %s seconds ---" % (end_time - start_time))